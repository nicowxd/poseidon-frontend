# syntax=docker/dockerfile:experimental
FROM poseidon-docker-dev.artifact.tecnalia.com/nodejs:master

ENV PORT 3000
ENV CHOKIDAR_USEPOLLING true
EXPOSE $PORT

RUN yarn build

CMD ["sh", "-c", "serve -s build -l $PORT"]
