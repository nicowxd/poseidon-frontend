describe('Revoke access for one PII', () => {
  before(() => {
    cy.login();
    cy.navigateTo(3, 'Data Processors');
  });

  it('Revokes access for the first name', () => {
    cy.waitForContent('PoSeID-on Demo Data Processor');
    cy.contains('PoSeID-on Demo Data Processor').click();

    cy.waitForContent('first name');
    // We need to force this because scrolling is broken in cypress (again)
    cy.contains('tr', 'first name').find('button').click({force: true});

    cy.get('.MuiSnackbar-root').contains('Permission revoked successfully');
    cy.screenshot();
    cy.get('.MuiSnackbar-root button[aria-label="close"]').click();

    cy.wait(2000);
    cy.waitForContent('last name');
    cy.get('main').should('not.contain', 'first name');
    cy.screenshot();
  });
});
