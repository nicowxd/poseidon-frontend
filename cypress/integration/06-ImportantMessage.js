describe('Important messages', () => {
  before(() => {
    cy.login();
    cy.navigateTo(2, 'Messages');
  });

  it('Checks if PDA or RMM is sending an important message', () => {
    cy.contains('Important').click();
    cy.waitForSelector('#content > div > div > section');
    cy.screenshot();
  });
  it('Clicks the message button', () => {
    cy.get('#content > div > div > section button').click();
    cy.waitForContent('Data Processors >'); // That's in the title.
    cy.screenshot();
  });
});
