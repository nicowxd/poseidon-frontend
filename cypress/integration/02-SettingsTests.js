const password = Cypress.env('password');
const newPassword = Cypress.env('changedPassword');

describe('Settings', () => {
  before(() => {
    cy.login();
    cy.navigateTo(1, 'Summary');
  });

  it('Opens the settings', () => {
    cy.openMenu();
    cy.get('#user-account-menu > div.MuiPaper-root.MuiMenu-paper.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul > div:nth-child(3) > a > li').click();
    cy.screenshot();
  });

  it('Changes the password without a new value', () => {
    cy.waitForSelector('#content > div > div > div > div > form > button > span.MuiButton-label');
    cy.get('#content > div > div > div > div > form > button > span.MuiButton-label').click();
    cy.contains('The passwords are the same value');
    cy.screenshot();
  });

  // TODO: run this test when blockchain API is fixed
  xit('Changes the password with a new value', () => {
    cy.waitForSelector('#content > div > div > div > div > form > div:nth-child(1) > div > input');
    cy.get('#content > div > div > div > div > form > div:nth-child(1) > div > input').type(password);
    cy.get('#content > div > div > div > div > form > div:nth-child(2) > div > input').type(newPassword);
    cy.get('#content > div > div > div > div > form > button > span.MuiButton-label').click();
    cy.waitForSelector('#content > div > div > div > div > form > textarea:nth-child(3)');
    cy.screenshot();
  });

  it('Opens the user menu and logs out', () => {
    cy.openMenu()
    cy.get('#user-account-menu > div.MuiPaper-root.MuiMenu-paper.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul > li').click();
    cy.screenshot();
  });

  it('Logs in with changed password', () => {
    cy.login(password); // TODO: change to newPassword once test actually works
    cy.navigateTo(1, 'Summary');
    cy.screenshot();
  });

  it('Opens the menu and navigates to the settings page', () => {
    cy.openMenu();
    cy.get('#user-account-menu > div.MuiPaper-root.MuiMenu-paper.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul > div:nth-child(3) > a > li').click();
    cy.screenshot();
  });

  // TODO: run this test when blockchain API is fixed
  xit('Changes password back again', () => {
    cy.waitForSelector('#content > div > div > div > div > form > div:nth-child(1) > div > input');
    cy.get('#content > div > div > div > div > form > div:nth-child(1) > div > input').type(newPassword);
    cy.get('#content > div > div > div > div > form > div:nth-child(2) > div > input').type(password);
    cy.get('#content > div > div > div > div > form > button > span.MuiButton-label').click();
    cy.waitForSelector('#content > div > div > div > div > form > textarea:nth-child(3)');
    cy.screenshot();
  });
});
