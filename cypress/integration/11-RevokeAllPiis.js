describe('Revoke all PIIs', () => {
  before(() => {
    cy.login();
    cy.navigateTo(3, 'Data Processors');
  });

  it('Revokes access to all PIIs', () => {
    cy.waitForContent('PoSeID-on Demo Data Processor');
    cy.contains('PoSeID-on Demo Data Processor').click();

    cy.waitForContent('national id number');
    cy.contains('button', 'Revoke all permissions').click({force: true});

    cy.get('.MuiSnackbar-root').contains('Value is protected');
    cy.screenshot();
    cy.get('.MuiSnackbar-root button[aria-label="close"]').click();

    // TODO: this should not need to happen. This is a bug.
    cy.reload();

    cy.waitForContent('national id number');
    cy.get('main').should('not.contain', 'last name');
    cy.screenshot();
  });
});
