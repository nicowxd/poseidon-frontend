describe('Revoke protected PII', () => {
  before(() => {
    cy.login();
    cy.navigateTo(3, 'Data Processors');
  });

  it('Revoke access for specified protected PII', () => {
    cy.waitForContent('PoSeID-on Demo Data Processor');
    cy.contains('PoSeID-on Demo Data Processor').click();

    cy.waitForContent('national id number');
    // We need to force this because scrolling is broken in cypress (again)
    cy.contains('tr', 'national id number').find('button').click({force: true});

    cy.get('.MuiSnackbar-root').contains('Value is protected');
    cy.screenshot();
    cy.get('.MuiSnackbar-root button[aria-label="close"]').click();
  });
});

