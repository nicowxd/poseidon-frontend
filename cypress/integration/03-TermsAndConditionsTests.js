describe('Terms and Conditions', () => {
  before(() => {
    cy.login();
    cy.navigateTo(1, 'Summary');
  });

  it('Opens menu and navigates to the terms and conditions page', () => {
    cy.openMenu();
    cy.get('#user-account-menu > div.MuiPaper-root.MuiMenu-paper.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul > div:nth-child(1) > a > li').click();
    cy.screenshot();
  });

  it('Opens the terms and conditions page', () => {
    cy.waitForSelector('#content > div > div:nth-child(1) > div > div > div:nth-child(1) > div');
    cy.get('#content > div > div:nth-child(1) > div > div > div:nth-child(1) > div').click();
    cy.get('#content > div > div:nth-child(1) > div > div > div:nth-child(2) > div').scrollIntoView().click();
    cy.get('#content > div > div:nth-child(1) > div > div > div:nth-child(3) > div').scrollIntoView().click();
    cy.get('#content > div > div:nth-child(1) > div > div > div:nth-child(4) > div').scrollIntoView().click();
    cy.screenshot();
  });
});
