describe('Dashboard', () => {
  beforeEach(() => {
    cy.login();
    cy.navigateTo(1, 'Summary');
  });

  it('Has all content on the dashboard page', () => {
    cy.waitForSelector('#content > div > div:nth-child(3) > div > div > div:nth-child(1)');
    cy.contains('Summary');
    cy.contains('Exchange reports');
    cy.contains('Messages');
    cy.contains('Dashboard');
    cy.contains('Data Processors');
    cy.contains('Help');
    cy.scrollTo('bottom');
    cy.screenshot();
  });

  it('Has all the options available in the menu', () => {
    cy.openMenu();
    cy.contains('Terms & Conditions');
    cy.contains('Privacy policy');
    cy.contains('Settings');
    cy.contains('Logout');
    cy.screenshot();
  });
});
