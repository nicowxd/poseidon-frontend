describe('Granting permission from message', () => {
  var removablePiis = Cypress.env('removablePiis');
  var nonRemovablePiis = Cypress.env('nonRemovablePiis');

  beforeEach(() => {
    cy.login();
    cy.navigateTo(2, 'Messages');
    cy.contains('PoSeID-on Demo Data Processor wants to request permission').click();
    cy.waitForSelector('#content > div > div > section');
  });

  it('Lists the fields requested', () => {
    removablePiis.forEach((pii) => {
      cy.get(`#content > div > div > section input#${pii}`).should('be.visible');
    });
    nonRemovablePiis.forEach((pii) => {
      cy.get(`#content > div > div > section input#${pii}`).should('be.visible');
    });

    cy.screenshot();
  });

  it('Fills out the fields and submits', () => {
    removablePiis.forEach((pii, i) => {
      cy.get(`#content > div > div > section input#${pii}`).focus().type('asdfg');
    });
    nonRemovablePiis.forEach((pii) => {
      cy.get(`#content > div > div > section input#${pii}`).focus().type('asdfg');
      cy.get('.MuiSnackbar-root').contains('Value is protected');
      cy.get('.MuiSnackbar-root button[aria-label="close"]').click();
    });
    cy.contains('Grant access').click();
    cy.waitForContent('Permission granted successfully');
    cy.screenshot();
    cy.waitForContent('Data Processors > PoSeID-on Demo Data Processor');
  });

  // it('Grants DP permission via directly supplying PII or via another DP', () => {

  //   if (msg.text().includes('PoSeID-on Demo Data Processor')) {
  //     cy.waitForSelector('#content > div > div > section > div:nth-child(3) > div > div');
  //     cy.get('#content > div > div > section > div:nth-child(3) > div > div').each((el, index) => {
  //       if (index + 1 !== 1) {
  //         if (el.text().includes('Switch the toggle to change input data manuallySelect the data processor that will provide this information')) {
  //           cy.get(`#content > div > div > section > div:nth-child(3) > div > div:nth-child(${index + 1}) > div > div > span > span.MuiButtonBase-root.MuiIconButton-root.PrivateSwitchBase-root-494.MuiSwitch-switchBase.MuiSwitch-colorPrimary > span.MuiIconButton-label > input`).click();
  //           cy.get(`#content > div > div > section > div:nth-child(3) > div > div:nth-child(${index + 1}) > div > div > div > div > input`).type('test-info');
  //         } else {
  //           cy.get(`#content > div > div > section > div:nth-child(3) > div > div:nth-child(${index + 1}) > div > div > div > input`).focus().type('test-info');
  //         }
  //       } else {
  //         if (el.text().includes('Switch the toggle to change input data manuallySelect the data processor that will provide this information')) {
  //           cy.get('#selectDP').then(($val) => {
  //             let elId = $val.text();
  //             let selector = (`#menu-${elId}`).replace(' ', '_');
  //             cy.get(`#content > div > div > section > div:nth-child(3) > div > div:nth-child(${index + 1}) > div > div > div > div`).click().then(() => {
  //               cy.get(`${selector} > div.MuiPaper-root.MuiMenu-paper.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul > li:nth-child(1)`).click();
  //             });
  //           });
  //         } else {
  //           cy.get(`#content > div > div > section > div:nth-child(3) > div > div:nth-child(${index + 1}) > div > div > div > input`).focus().type('test-info');
  //         }
  //       }
  //     });
  //     cy.waitForSelector('#content > div > div > section > div:nth-child(3) > button');
  //     cy.get('#content > div > div > section > div:nth-child(3) > button').focus().click({ force: true });
  //   } else {
  //     console.log('test cannot run because PoSeID-on Demo Data Processor message is missing ');
  //   }
  // });
});
