describe('Warning message', () => {
  before(() => {
    cy.login();
    cy.navigateTo(2, 'Messages');
  });

  it('Checks if a warning message is present', () => {
    cy.contains('Warning').click();
    cy.waitForSelector('#content > div > div > section');
    cy.screenshot();
  });

  it('Opens that message', () => {
    cy.get('#content > div > div > section button').click();
    cy.waitForSelector('#content > div > h1');
    cy.get('#content > div > h1').contains('Data Processor');
    cy.screenshot();
  });
});
