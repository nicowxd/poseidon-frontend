const localhost = Cypress.env('generalPath');
const stub = Cypress.env('requestPermissionName');

describe('Permission requests', () => {
  it('Requests permission', () => {
    cy.login();
    cy.navigateTo(2, 'Messages'); // Otherwise login is not complete
    cy.visit(`${localhost}/poseidon-dp/`);
    cy.contains('PoSeID-on Demo Data Processor');
    cy.get('#name').type(stub);
    cy.get('body > form > fieldset > input[type=submit]:nth-child(5)').click();
    cy.wait(5000); // At this point, the correlation message is not sent if we navigate away immediately
  });
  it('Receives a message', () => {
    cy.navigateTo(2, 'Messages');
    cy.contains('#content', 'PoSeID-on Demo Data Processor wants to request permission', {timeout: 30000}); // Yes, it can take that long
    cy.screenshot();
  });
});
