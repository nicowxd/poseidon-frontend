describe('Change PII value for DP', () => {
  before(() => {
    cy.login();
    cy.navigateTo(3, 'Data Processors');
  });

  it('Changes the PII value for a field', () => {
    cy.waitForContent('PoSeID-on Demo Data Processor');
    cy.contains('PoSeID-on Demo Data Processor').click();
    // We need to select on value because these inputs don't have IDs for some
    // inexplicable reason.
    cy.waitForSelector('#content input[value="asdfg"]');
    cy.get('#content input[value="asdfg"]').last().clear().type('qwerty');
    cy.get('.MuiSnackbar-root').contains('Field updated successfully');
    cy.screenshot();
    cy.get('.MuiSnackbar-root button[aria-label="close"]').click();
  });
});
