const localhost = Cypress.env('generalPath');
const alice = Cypress.env('aliceUserName');
const password = Cypress.env('password');

Cypress.Commands.add('waitForSelector', (selector) => {
  cy.get(selector, { timeout: 60000 }).should('be.visible');
})

Cypress.Commands.add('waitForContent', (text) => {
  cy.contains(text, { timeout: 60000 }).should('be.visible');
})

Cypress.Commands.add('login', (p) => {
  var registered = false;

  // If we're already logged in, log out
  cy.clearCookie('poseidontoken');

  cy.visit(localhost);
  cy.contains('Stub Login');
  cy.waitForSelector('.dev_button');
  cy.get('.dev_button').first().click();
  cy.waitForSelector('#auth_id');
  cy.get('#auth_id').select(alice);
  cy.get('input').click();
  cy.get('body > div.MuiDialog-root > div.MuiDialog-container.MuiDialog-scrollPaper > div > div > form > button > span.MuiButton-label').then(el => {
    if (el.text().includes('Register')) {
      cy.get('input').eq(0).type(password);
      cy.get('input').eq(1).type(password);
      cy.get('body > div.MuiDialog-root > div.MuiDialog-container.MuiDialog-scrollPaper > div > div > form > div:nth-child(3) > div:nth-child(2) > div > div > span > span.MuiIconButton-label > input').click();
      registered = true;
    } else {
      cy.get('input').type(p || password);
    }
  });
  cy.get('button').eq(1).click();
  if (registered) {
    cy.contains('This passphrase is showing only once')
  }
});

Cypress.Commands.add('openMenu', () => {
  cy.get('#root > div > div > div > svg.MuiSvgIcon-root.customUserIcon.MuiSvgIcon-fontSizeLarge').click();
  cy.waitForSelector('#user-account-menu > div.MuiPaper-root.MuiMenu-paper.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul');
});

Cypress.Commands.add('navigateTo', (index, title) => {
  cy.waitForSelector(`#root > div > div > div > a:nth-child(${index})`);
  cy.get(`#root > div > div > div > a:nth-child(${index})`).click();
  cy.contains(title);
});
