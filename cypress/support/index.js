import './commands';
import 'cypress-mochawesome-reporter/register';

Cypress.Screenshot.defaults({
    capture: 'runner'
});
Cypress.Cookies.debug(true, { verbose: false });
Cypress.Cookies.defaults({
    preserve: 'poseidontoken'
});
