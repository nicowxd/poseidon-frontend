BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
REPOSITORY = poseidon-docker-dev.artifact.tecnalia.com
NAME = $(shell basename `pwd`)
DOCKER_BUILDKIT = 1

.EXPORT_ALL_VARIABLES:

.PHONY: build push check clean

all: build

build: check
	docker pull poseidon-docker-dev.artifact.tecnalia.com/nodejs:master
	docker build --no-cache --ssh default --build-arg branch=$(BRANCH) -t $(REPOSITORY)/$(NAME):$(BRANCH) .

push:
	docker push $(REPOSITORY)/$(NAME):$(BRANCH)

check:
	docker run --rm -i hadolint/hadolint < Dockerfile

clean:
	-docker rm `docker ps -a -q  --filter "status=exited"`
	-docker rmi `docker images -q --filter "dangling=true"`
