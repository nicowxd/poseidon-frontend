import {all, call, fork, put, takeLatest} from 'redux-saga/effects';
import {putHttp} from 'utils/http';
import {API_KEYS_USER} from 'constants.js';
import {CHANGE_PASSWORD, RECOVERY_PASSPHRASE} from '../../reducers/settings';

export function* watchChangePassword() {
  yield takeLatest(CHANGE_PASSWORD, changePassword);
}
function* changePassword(params) {

  const body = new FormData();
  body.append('passphrase', params.payload.passphrase);
  body.append('new_passphrase', params.payload.newPassphrase);

  const response = yield call(putHttp, API_KEYS_USER, body);

  yield put({
    type: RECOVERY_PASSPHRASE,
    payload: { recovery: response.recovery }
  });
}

export default function* root() {
  yield all([
    fork(watchChangePassword)
  ]);
}
