import {all, call, fork, put, select, takeLatest} from 'redux-saga/effects';
import {handleBackendError} from 'utils/errorHandling';
import {getOrganisation as selectOrganisation} from 'utils/selectors/organisation';
import {deleteHttp, get, putHttp} from 'utils/http';
import { normalizeToState } from 'utils/normalizers/fields';
import {API_DATA_PROCESSORS, API_PERMISSIONS} from 'constants.js';
import {
  ORGANISATION_GET_DATA,
  ORGANISATION_STORE_DATA,
  REVOKE_PERMISSION,
  REVOKE_ALL_PERMISSIONS} from 'reducers/organisation';
import { addNotification } from 'reducers/notifications';
import {getProcessors } from 'sagas/organisations';
import {GET_PII_TYPES} from "../../reducers/organisation";
import {EDIT_PII, SET_PII, SET_FIELDS, GET_FIELDS } from "../../reducers/dataFields";

const selectOrganisations = state => state.organisations.processors || [];

function* getOrganisation({ certificate }) {
  try {
    const organisations = yield select(selectOrganisations);

    if (organisations.length === 0) {
      yield call(getProcessors);
    }
    const organisation = yield select(selectOrganisation, certificate);

    yield put({
      type: ORGANISATION_STORE_DATA,
      payload: { ...organisation }
    })

  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* revokePermission(props) {
  try {
    yield call(deleteHttp, `${API_DATA_PROCESSORS}/${props.certificate}/${props.field}`)
    yield call(deleteHttp, `${API_PERMISSIONS}/${props.certificate}/${props.field}`)
    yield put(addNotification({
      id: 'successful',
      autoClear: false,
      message: "Permission revoked successfully",
      messageId: 'api.success',
    }))
    setTimeout(() => { window.location.reload(); }, 5000);
  } catch (err) {
    const error = yield err || {};
    const demoError = {status: 404, error: error.error}
    yield put(handleBackendError(demoError));
  }
  yield put({
    type: GET_FIELDS,
    certificate: props.certificate
  });
}

/*eslint-disable */
function* getPiiTypes(props) {
  const piiObjects = {};
  const pii_array = [];
  try {
    let piiTypeArray = yield call(get, `${API_DATA_PROCESSORS}/${props.payload.certificate}`);

    for (let piiType of piiTypeArray.fields) {
      let piiValue = yield call(get, `${API_DATA_PROCESSORS}/${props.payload.certificate}/${piiType}`);
      piiObjects[piiType] = piiValue.value;
      pii_array.push(piiValue);
    }
  } catch (err) {
      const error = yield err || {};
      if(error.status === 404){
        yield put(handleBackendError({error: "Currently no data is shared with this Data Processor", status:404}));
      } else {
        yield put(handleBackendError(error));
      }
  }

    yield put({
    type: SET_PII,
    payload: { pii_array, piiObjects }
  })
}
/*eslint-enable */

function* editPii(param) {
  // Due to some weird bug, editPii is sometimes called with undefined. Nobody
  // can find why, so this is a quick fix.
  if (param.payload.pii === undefined) {
    return;
  }

  const body = new FormData();
  body.append('value', param.payload.pii);
  try{
    yield call(putHttp, `${API_DATA_PROCESSORS}/${param.payload.certificate}/${param.payload.field}`, body);
    yield put(addNotification({
      id: 'successful',
      autoClear: false,
      message: "Field updated successfully",
      messageId: 'api.success',
    }))
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* getFields(props){
  let fields
  let normalizedFields
  try {
    let fieldValues = []
    let fields = []
    let date = Date.now() * 0.001;
    let fieldNames = yield call(get, `${API_PERMISSIONS}/${props.certificate}`);

    for(let i in fieldNames){
      if(fieldNames[i].field){
        let newField = {
          name: fieldNames[i].field,
          display: fieldNames[i].field.replace("_", " "),
          value: "",
          state: fieldNames[i].state,
          deadline: fieldNames[i].deadline,
          data_processor: fieldNames[i].data_processor
        }
        if(newField.deadline > date  && newField.state === "granted"){
          fields.push(newField)
        }
      }
    }

    for (let index in fields){
      try{
        let value =  yield call(get, `${API_DATA_PROCESSORS}/${props.certificate}/${fields[index].name}`)
        fields[index].value = value.value
      } catch (err) {
      }
    }

    normalizedFields = normalizeToState(fields)
    yield put({
      type: SET_FIELDS,
      payload: { normalizedFields, fieldValues, fields}
    })
  } catch (err) {
    if(fields === undefined ||  fields.fields[0] !== ''){
      yield put(handleBackendError(yield err || {}));
    }
  }
}

function* revokeAllPermissions(props){
  try {
    yield call(deleteHttp, `${API_PERMISSIONS}/${props.cert}`)

    const organisations = yield select(selectOrganisations);
    if (organisations.length === 0) {
      yield call(getProcessors);
    }
    const organisation = yield select(selectOrganisation, props.cert);
    yield put({
      type: ORGANISATION_STORE_DATA,
      payload: { ...organisation }
    })
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* watchRevokeAllPermissions(){
  yield takeLatest(REVOKE_ALL_PERMISSIONS, revokeAllPermissions)
}

function* watchGetFields(){
  yield takeLatest(GET_FIELDS, getFields);
}

function* watchGetOrganisation() {
  yield takeLatest(ORGANISATION_GET_DATA, getOrganisation);
}
function* watchRevokePermission() {
  yield takeLatest(REVOKE_PERMISSION, revokePermission);
}

function* watchGetPiiTypes() {
  yield takeLatest(GET_PII_TYPES, getPiiTypes);
}

function* watchEditPii() {
  yield takeLatest(EDIT_PII, editPii);
}

export default function* root() {
  yield all([
    fork(watchGetOrganisation),
    fork(watchRevokePermission),
    fork(watchGetPiiTypes),
    fork(watchEditPii),
    fork(watchGetFields),
    fork(watchRevokeAllPermissions)
  ]);
}
