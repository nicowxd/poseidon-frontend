import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import {correlateDataProccessor} from 'sagas/correlate'
import { get } from 'utils/http';
import { handleBackendError } from 'utils/errorHandling';
import { normalizeToState } from 'utils/normalizers/dataProcessors';
import {
  API_DATA_PROCESSORS, API_PERMISSIONS
} from 'constants.js';
import {
  DATA_PROCESSORS_GET_PROCESSORS,
  DATA_PROCESSORS_SET_PROCESSORS
} from 'reducers/organisations';
import {SUMMARY_SET_SUMMARY} from "../../reducers/summary";




export function* getProcessors() {
  try {
    const response = yield call(get, API_DATA_PROCESSORS);

    yield put({
      type: DATA_PROCESSORS_SET_PROCESSORS,
      payload: normalizeToState(response)
    });

    const permissionsCount = yield call(get, API_PERMISSIONS);

    yield put({ type: SUMMARY_SET_SUMMARY,
      payload: {organisations: response.processors.length,
        requests: 0,
        permissions: permissionsCount.count
    }
    });
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
  yield call(correlateDataProccessor);
}

function* watchGetProcessors() {
  yield takeLatest(DATA_PROCESSORS_GET_PROCESSORS, getProcessors);
}

export default function* root() {
  yield all([
    fork(watchGetProcessors)
  ]);
};
