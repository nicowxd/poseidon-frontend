import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

import { get, post, deleteHttp, putHttp } from 'utils/http';
import { handleBackendError } from 'utils/errorHandling';
import { normalizeToState } from 'utils/normalizers/auth';
import {
  API_AUTH,
  API_KEYS_AUTHENTICATE,
  API_KEYS_USER,
  LOGIN_PATH,
} from 'constants.js';
import {
  AUTH_AUTHENTICATE,
  AUTH_CLEAR_STATE,
  AUTH_GET_STATUS,
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_SET_STATUS
} from 'reducers/auth';

function* authAuthenticate({ passphrase, register, recovery, recoveryPassphrase, firstLogin, firstRecovery }) {
  try {
    yield put({
      type: AUTH_SET_STATUS,
      payload: { isSubmitting: true }
    });
    const body = new FormData();

    if (recovery) {

      body.append('new_passphrase', passphrase);
      body.append('recovery_passphrase', recoveryPassphrase);
      yield call(putHttp, API_KEYS_USER, body);
      window.location.href = "/";

    } else  {

      body.append('passphrase', passphrase);
      const response = yield call(post, register ? API_KEYS_USER : API_KEYS_AUTHENTICATE, body);
      if(register){
        yield put({
          type: AUTH_SET_STATUS,
          payload: { firstLogin: true, firstRecovery: response.recovery }
        });
      }

      yield put({ type: AUTH_GET_STATUS });
    }

  } catch (err) {
    const error = yield err || {};

    if (error.status === 401) {
      yield put({
        type: AUTH_SET_STATUS,
        payload: {
          invalidPassphrase: true,
          isSubmitting: false
        }
      });
    } else {
      yield put({
        type: AUTH_SET_STATUS,
        payload: {
          isSubmitting: false
        }
      });
      yield put(handleBackendError(yield err || {}));
    }
  }
}

function authLogin() {
  window.location = LOGIN_PATH + "?redirect_to=" + encodeURIComponent(window.location.pathname + window.location.search);
}

function* authLogout() {
  try {
    yield call(deleteHttp, API_AUTH);
    yield put({ type: AUTH_CLEAR_STATE });
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* authGetStatus() {
  try{
    yield call(get, API_KEYS_USER);
  }catch (err) {
      yield put({
      type: AUTH_SET_STATUS,
      payload: {
        isRegistered: true
      }
    });
  }

  try {
    const authState = yield call(get, API_AUTH);
    const { authProvider, blockchain } = normalizeToState(authState);

    if (authProvider && blockchain) {
      const { certificate } = yield call(get, API_KEYS_USER);
      yield put({
        type: AUTH_SET_STATUS,
        payload: { certificate }
      });
    }

    yield put({
      type: AUTH_SET_STATUS,
      payload: { authProvider, blockchain }
    });
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }

}

function* watchAuthAuthenticate() {
  yield takeLatest(AUTH_AUTHENTICATE, authAuthenticate)
}

function* watchAuthGetStatus() {
  yield takeLatest(AUTH_GET_STATUS, authGetStatus);
}

function* watchAuthLogin() {
  yield takeLatest(AUTH_LOGIN, authLogin);
}

function* watchAuthLogout() {
  yield takeLatest(AUTH_LOGOUT, authLogout);
}

export default function* root() {
  yield all([
    fork(watchAuthAuthenticate),
    fork(watchAuthGetStatus),
    fork(watchAuthLogin),
    fork(watchAuthLogout)
  ]);
}

