import { all, call, fork, takeLatest } from 'redux-saga/effects';

import { post } from 'utils/http';

import {
  API_DATA_PROCESSORS_CORRELATE
} from 'constants.js';

export function* correlateDataProccessor() {

  const searchParams = new URLSearchParams(window.location.search)

  if (searchParams.has('processor') && searchParams.has('correlation_id')){

    //getting the correlation_id and certificate from actuall link
    const correlation_id = searchParams.get('correlation_id');
    const certificate= searchParams.get('processor');
    const postLink = API_DATA_PROCESSORS_CORRELATE + "/" + certificate;
    const body = new FormData();

    body.append("correlation_id", correlation_id);

    //creating post form for corelation
    yield call(post, postLink, body);
    }
  }
  
function* watchCorrelateDataProccessor (){
    yield takeLatest(API_DATA_PROCESSORS_CORRELATE, correlateDataProccessor)
}

export default function* root() {
    yield all([
        fork(watchCorrelateDataProccessor)
    ]);
  }