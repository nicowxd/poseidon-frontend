import { all, fork, put, takeLatest, call } from 'redux-saga/effects';
import { putHttp } from 'utils/http';
import { handleBackendError } from 'utils/errorHandling';
import { addNotification } from 'reducers/notifications';
import {
  API_PERMISSIONS,
  API_DATA_PROCESSORS,
  MSG_PROTECTED_PII
 } from 'constants.js'
import {
  GRANT_ACCESS_PII,
  UPDATE_ACCESS_PII
} from 'reducers/messageContent';

function* updatePii(params){
  let values = true;
  for (let i in params.piiArray){
    let sharedInput = params.sharePii.find(name => name.field === params.piiArray[i]);

    if(sharedInput === undefined){
      values = false
    }
  }

  if (values === false){
    yield put(handleBackendError({error: "Please Select or Input a value for the requested data", status:404}));
  } else {

    const body = {
      sender:  params.sharePii[0].value
    }

    try {
      yield call(putHttp, `${API_PERMISSIONS}/${params.dpCertificate}/${params.field}`, JSON.stringify(body))
      if ( params.redirectUrl !== undefined ) {
        window.location.href = params.redirectUrl;
      } else {
        yield put(addNotification({
          id: 'successful',
          autoClear: false,
          message: "Permission granted successfully, you will be redirected to the data processor page",
          messageId: 'api.success',
        }))
      }
    } catch (err){
      yield put(handleBackendError(yield err || {}));
    }
  }
}

function* grantAccessPii(params) {
  let error = false;
  let values = true;
  for (let i in params.piiArray){
    let directInput = params.directPii.find(name => name.field === params.piiArray[i]);
    let sharedInput = params.sharePii.find(name => name.field === params.piiArray[i]);
    if((!(sharedInput !== undefined || (directInput !== undefined && directInput.value !== undefined))) && params.piiArray[i] !== "poseidon_transactions"){
        values = false
    }
  }

  if (values === false){
    yield put(handleBackendError({error: "Please Select or Input a value for the requested data", status:404}));
  } else {
    for (let i in params.piiArray){
      const piiValue = new FormData();
      const direct = params.directPii[i];

      if (!direct) {
        continue;
      }

      piiValue.append('value', direct.value);

      try {
        yield call(putHttp, `${API_PERMISSIONS}/${params.dpCertificate}/${direct.field}`)
        yield call(putHttp, `${API_DATA_PROCESSORS}/${params.dpCertificate}/${direct.field}`, piiValue)
      } catch (err){
        if(direct?.value !== params.initialPiiState[i]?.value){
          yield put(handleBackendError(yield err || {}));
        }
      }
    }

    for ( let i in params.sharePii) {
      const share = params.sharePii[i];

      if (!share) {
        continue;
      }

      const body = {
        sender: share.value
      }

      try {
        yield call(putHttp, `${API_PERMISSIONS}/${params.dpCertificate}/${params.sharePii[i].field}`, JSON.stringify(body))
      } catch (err){
        error = true;
        yield put(handleBackendError(yield err || {}));
      }
    }

    if( error === false){
      if ( params.redirectUrl !== undefined ) {
        window.location.href = params.redirectUrl;
      } else {
        setTimeout(() => {
          window.location.pathname = "/organisations/" + params.dpCertificate;
        }, 3000);
        yield put(addNotification({
          id: 'successful',
          autoClear: false,
          message: "Permission granted successfully, you will be redirected to the data processor page",
          messageId: 'api.success',
        }))
      }
    }
  }
}

function* watchGrantAccessPii(){
  yield takeLatest(GRANT_ACCESS_PII, grantAccessPii)
}

function* watchUpdatePii(){
  yield takeLatest(UPDATE_ACCESS_PII, updatePii)
}

export default function* root() {
  yield all([
    fork(watchGrantAccessPii),
    fork(watchUpdatePii)
  ]);
}
