import { all, fork, put, takeLatest, call } from 'redux-saga/effects';
import axios from 'axios'
import { get } from 'utils/http';
import { handleBackendError } from 'utils/errorHandling';
import { normalizeToState } from 'utils/normalizers/messages';
import {  
  API_MESSAGES, 
  // API_PERMISSIONS, 
  API_GET_DP_LIST } from 'constants.js'
import {
  MESSAGES_GET_MESSAGES,
  MESSAGES_SET_MESSAGES,
  MESSAGES_GET_NR_MESSAGES,
  MESSAGES_SET_NR_MESSAGES,
  MESSAGES_GET_NR_PAGES,
  MESSAGES_SET_NR_PAGES,
  MARK_AS_READ,
  UPDATE_MESSAGE_ID_ARRAY,
  GET_LIST_OF_DATA_PROCESSORS,
  SET_LIST_OF_DATA_PROCESSORS,
  SET_CURRENT_PAGE,
  GET_CURRENT_PAGE
} from 'reducers/messages';

function* getMessages(params) {
  try {
    const response = yield call(get, API_MESSAGES  + `?count=${params.itemsPerPage}&page=${params.page}`);

    yield put({
      type: MESSAGES_SET_MESSAGES,
      payload: normalizeToState(response),
    });
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* getNrOfMessages(){
  try {
/*eslint-disable */
    const response = yield call(get, API_MESSAGES);
    let unrMsg = 0;
    for (let unread in response.messages){
      if(response.messages[unread].read === false){
        unrMsg++;
      }
    }
/*eslint-enable */

    
    yield put({
      type: MESSAGES_SET_NR_MESSAGES,
      payload: unrMsg,
    });
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* getNrOfPages() {
  try {
    const response = yield call(get, API_MESSAGES);

    let unrMsg = 0;
    for (let unread in response.messages){
      if(response.messages[unread].read === false){
        unrMsg++;
      }
    }

    let pages = response.pages;
    yield put({
      type: MESSAGES_SET_NR_PAGES,
      payload: {pages, unrMsg}
    })

  }catch (err) {
    yield put(handleBackendError(yield err || {}))
  }
}

function* markMessageAsRead(action) {
  try {
    const body = new FormData();
    body.append('id', action.id);
    const API_MESSAGES_READ = API_MESSAGES + `/${action.id}`
    yield put({
      type: UPDATE_MESSAGE_ID_ARRAY,
      payload: action.id,
    });
    axios.put(API_MESSAGES_READ);
  } catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

// function* giveAccess(dp, field){
//   try {
//     yield call(put, API_PERMISSIONS + '/' + dp  + '/' + field);
//   } catch (err) {
//     yield put(handleBackendError(yield err || {}));
//   }
// }

function* getListOfDataProcessor(props){
  try{
    const dataProcessors = yield call (get, `${API_GET_DP_LIST}/${props.field}`);
    const keys = Object.keys(dataProcessors);
    const dataProcessorsCertificates= [];
    for( let i in keys){
      if (keys[i] !== 'status'){
        dataProcessorsCertificates.push(keys[i])
      }
    }
    yield put({
      type: SET_LIST_OF_DATA_PROCESSORS,
      payload: dataProcessorsCertificates,
    });
  }catch (err) {
    yield put(handleBackendError(yield err || {}));
  }
}

function* getCurretnPage(props){
  let currentPage= parseInt(props.page)
  yield put({
    type: SET_CURRENT_PAGE,
    payload: currentPage,
  });
}

function* watchGetNrOfMessages() {
  yield takeLatest(MESSAGES_GET_NR_MESSAGES, getNrOfMessages);
}

function* watchGetMessages() {
  yield takeLatest(MESSAGES_GET_MESSAGES, getMessages);
}

function* watchMarkMessageAsRead() {
  yield takeLatest(MARK_AS_READ, markMessageAsRead);
}

function* watchGetNrOfPages(){
  yield takeLatest(MESSAGES_GET_NR_PAGES, getNrOfPages)
}

// function* watchGiveAccess(){
//   yield takeLatest(GIVE_ACCESS, giveAccess)
// }

function* watchGetListOfDataProcessor(){
  yield takeLatest(GET_LIST_OF_DATA_PROCESSORS, getListOfDataProcessor)
}

function* warchGetCurretnPage(){
  yield takeLatest(GET_CURRENT_PAGE, getCurretnPage)
}

export default function* root() {
  yield all([
    fork(watchGetMessages),
    fork(watchMarkMessageAsRead),
    fork(watchGetNrOfMessages),
    fork(watchGetNrOfPages),
    fork(watchGetListOfDataProcessor),
    fork(warchGetCurretnPage)
  ]);
}
