import { all, fork } from 'redux-saga/effects';

import auth from './auth';
import organisation from './organisation';
import organisations from './organisations';
import messages from './messages';
import settings from './settings';
import correlate from './correlate'
import messageContent from './messageContent'

export default function* root() {
  yield all([
    fork(auth),
    fork(organisation),
    fork(organisations),
    fork(messages),
    fork(settings),
    fork(correlate),
    fork(messageContent)
  ]);
}
