import { connect } from 'react-redux';

import AuthWrapper from 'components/AuthWrapper';
import {
  authenticate,
  getAuthStatus,
  resetValidation,
  checkRegistered,
} from 'reducers/auth';

export default connect(
  ({ auth: {
    authProvider,
    blockchain,
    invalidPassphrase,
    isSubmitting,
    isRegistered,
    firstLogin,
    firstRecovery,
  } }) => ({
    authProvider,
    blockchain,
    invalidPassphrase,
    isSubmitting,
    isRegistered,
  }),
  {
    getStatus: getAuthStatus,
    resetValidation,
    submitPassphrase: authenticate,
    checkRegistered,
  },
)(AuthWrapper);

