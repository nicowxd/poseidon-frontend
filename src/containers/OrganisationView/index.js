import { connect } from 'react-redux';
import OrganisationView from 'components/OrganisationView';
import { clearOrganisationData, getOrganisationData, onRevoke, revokeAllPermissions } from 'reducers/organisation';
import { getFields } from 'reducers/dataFields';


export default connect(
  ({ organisation }, { match: { params: { certificate }} }) => ({
    certificate,
    ...(organisation.certificate.length > 0 && organisation.certificate === certificate)
      ? organisation
      : {}
  }),
  {
    onClearData: clearOrganisationData,
    onGetData: getOrganisationData,
    onRevoke: onRevoke,
    getFields: getFields,
    revokeAllPermissions
  }
)(OrganisationView);
