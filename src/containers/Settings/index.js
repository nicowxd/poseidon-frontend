import { connect } from 'react-redux';

import Settings from '../../components/Settings';

export default connect(
  ({ settings: { recoveryPassphrase } }) => ({ recoveryPassphrase }),
)(Settings);
