import { connect } from 'react-redux';

import MessageCard from 'components/MessageCard';

export default connect(
  (
    { messages: { messages }},
    { id }
  ) => ({
    ...messages.find(item => item.id === id)
  })
)(MessageCard);
