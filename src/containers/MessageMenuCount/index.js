import {connect} from 'react-redux';

import MessageMenuCount from 'components/MessageMenuCount';
import { getMessages, getNrOfMessages} from 'reducers/messages'

export default connect(
  ({ messages: { nrOfMessages, currentPage } }) => ({
    nrOfMessages, 
    currentPage,
  }),
  {
    getMessages: getMessages,
  }
)(MessageMenuCount);
