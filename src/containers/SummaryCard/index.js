import { connect } from 'react-redux';

import SummaryCard from 'components/SummaryCard';
import { getSummary } from 'reducers/summary';

export default connect(
  ({ summary: { requests, organisations, permissions } }) => ({ requests, organisations, permissions }),
  { getSummary }
)(SummaryCard);
