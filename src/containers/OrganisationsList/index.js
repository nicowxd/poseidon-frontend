import { connect } from 'react-redux';

import OrganisationsList from 'components/OrganisationsList';
import { getOrganisations } from 'reducers/organisations';

export default connect(
  ({ organisations }) => ({ organisations: organisations.processors }),
  { getOrganisations }
)(OrganisationsList);
