import { connect } from 'react-redux';
import { getOrganisations } from 'reducers/organisations';
import SelectDataProcessor from 'components/SelectDataProcessor';
import {
    getDataProcessorList
} from 'reducers/messages';


export default connect(
  ( { messages, organisations} ) => ({
    listOfDataProcessors: messages.listOfDataProcessors,
  }),
  { onGetReports: getOrganisations,
    getDataProcessorList: getDataProcessorList,
  }
)(SelectDataProcessor);

