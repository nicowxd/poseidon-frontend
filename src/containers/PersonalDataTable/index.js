import { connect } from 'react-redux';
import PersonalDataTable from 'components/PersonalDataTable';


export default connect(
   ({ dataFields: {fieldValue, fields, pii_array, piiObjects, permissions } }) => ({ 
      fieldValue, 
      fields,
      pii_array, 
      piiObjects,
      permissions,
   }),{})(PersonalDataTable);