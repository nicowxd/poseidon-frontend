import { connect } from 'react-redux';

import ExchangerReports from 'components/ExchangeReports';
// TODO: This is just temporary to have some sort of data
import { getOrganisations } from 'reducers/organisations';

export default connect(
  ({ organisations }) => ({
    // TODO: Temp
    reports: organisations.processors
  }),
  { onGetReports: getOrganisations }
)(ExchangerReports);
