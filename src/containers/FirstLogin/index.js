import { connect } from 'react-redux';
import FirstLogin from 'components/FirstLogin';


export default connect(
  ({ auth: { firstLogin, firstRecovery } }) => ({ firstLogin, firstRecovery }))(FirstLogin);
