import { connect } from 'react-redux';

import MessagesList from 'components/MessagesList';
import { getMessages, markMessageAsRead, getNrOfPages } from 'reducers/messages';
import { history } from 'utils/history';
import { getOrganisations } from 'reducers/organisations';

import { MESSAGES_PATH } from 'constants.js';

export default connect(
  ({ messages: { pages, messages, nrOfMessages, messageIds, organisations } }, { activeId }) => ({
    nrOfMessages,
    pages,
    messageIds,
    messages: activeId
      ? messages.map(message => message.id === activeId
        ? { ...message, active: true }
        : message
      )
      : messages
  }),
  (dispatch) => ({
    getNrOfPages: () => (dispatch(getNrOfPages())),
    getOrganisations : () => (dispatch(getOrganisations())),
    getMessages: () => { dispatch(getMessages()); },
    onOpenMessage: (id) => { history.push(`${MESSAGES_PATH}/${id}`);
    dispatch(markMessageAsRead(id)) }
  })
)(MessagesList);
