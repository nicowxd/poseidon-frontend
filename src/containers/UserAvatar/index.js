import { connect } from 'react-redux';

import UserAvatar from 'components/UserAvatar';

export default connect(
  ({ user: { firstName, lastName } }) => ({ name: `${firstName} ${lastName}` })
)(UserAvatar);
