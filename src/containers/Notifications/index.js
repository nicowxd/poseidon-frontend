import { connect } from 'react-redux';

import Notifications from 'components/Notifications';
import { removeNotification } from 'reducers/notifications';

export default connect(
  ({ notifications }) => ({ notifications }),
  { onClose: removeNotification }
)(Notifications);
