const proxy = require('http-proxy-middleware');

// TODO: should read from env file
const API_HOST = `https://poseidon.localdev`;

module.exports = function(app) {
  app.use(proxy(['/auth', '/api', '/poseidon-dp'], {
    target: API_HOST,
    changeOrigin: true,
    router: {
      // when request.headers.host == 'localhost:3000',
      // override target 'http://www.example.org' to 'http://localhost:3000'
      'localhost:3000': API_HOST
    },
    secure: false
  }));
};
