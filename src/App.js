import React from "react";
import { Route, Switch } from "react-router-dom";
import StylesProvider from "@material-ui/styles/StylesProvider";

import AuthHandler from "containers/AuthHandler";
import Notifications from "containers/Notifications";
import Layout from "components/Layout";
import LinearGradientDef from "components/LinearGradientDef";

import Dashboard from "pages/Dashboard";
import Organisations from "pages/Organisations";
import Messages from "pages/Messages";
import Help from "pages/Help";
import TermsAndConditions from "pages/TermsAndConditions";
import Settings from "pages/Settings";

import fake from "./FakeDataProcesorForDemo/index"

import {
  HELP_PATH,
  MESSAGES_PATH,
  ORGANISATIONS_PATH,
  ROOT_PATH,
  TERMS_PATH,
  SETTINGS_PATH
} from "constants.js";

const App = () => (
  <StylesProvider injectFirst>
    <LinearGradientDef />
    <Notifications />
    <AuthHandler>
      <Layout>
        <Switch>
          <Route path={ROOT_PATH} exact component={Dashboard} />
          <Route path={ORGANISATIONS_PATH} component={Organisations} />
          <Route path={`${MESSAGES_PATH}/:id?`} component={Messages} />
          <Route path={HELP_PATH} component={Help} />
          <Route path={TERMS_PATH} component={TermsAndConditions} />
          <Route path={SETTINGS_PATH} component={Settings} />
          {/* remove this after testing */}
          <Route path="/fakeDataProcessor" component={fake} />
        </Switch>
      </Layout>
    </AuthHandler>
  </StylesProvider>
);

export default App;
