import React from 'react';
import styles from "./index.module.scss";
import logo from 'assets/images/poseidon_logo_sm.png';
import axios from 'axios';
import Button from 'components/Button'

export default () => {

  function setRedirect (){
    let certificate =  "CiA4wbIM8RpFMs4EnRA8DXIFFOz32aUsDkjnxRGzZO2xHBJApmKok7NOUST4sc7tEmE3P8MX4tA22SKVCewhUnmO4YPLUy_ZdsxjWvvbPFS0-ukZ2YQTAlHxV2gXuUtgEJzwCA==";
    let correlation_id = "correlation_id_string"
    axios.get("https://localhost:3000?processor=" + certificate + "correlation_id=" + correlation_id,
      ).then((result) => {
      if(result.status === 200) {
        window.location.href = "/?processor=" + certificate + "&correlation_id=" + correlation_id
      } else {
        console.log("Poseidon is down");
      }
    })
  }
  return (
    <div className={styles.flex}>
      <h1> Fake Data Processor </h1>
       <div className={styles.center}>
        <Button
            variant="contained"
            color="default"
            size="large"
            type="button"
            onClick={setRedirect}
        >Grant Access to
              <img className={styles.logo} src={logo} alt="Poseidon Logo"/>
        </Button>
      </div>
    </div>

  )
}


