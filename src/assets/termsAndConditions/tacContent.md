## __Annex I – Terms and Conditions__

The following Terms and Conditions (hereinafter: the “Terms”) govern the relations between PoSeID-on project and its provided services through the site poseidon.dei.uc.pt (hereinafter “the Platform”) and the users (individuals, private and public organizations) (hereinafter: the “User” individually and collectively the “Users”). PoSeID-on Platform is a project financed by the European Commission within the European Union’s Horizon 2020 program under Grant Agreement n° 786713. The PoSeID-on project has been created by a Consortium participated by several parties, from different European countries, such as SMEs, private/public IT providers, research centers and universities. PoSeID-on is a Platform aimed for the protection and control of personal data and secured information by means of a privacy enhanced dashboard (PED) to safeguard data subjects’ rights and support public and private organizations ensuring them GDPR compliance for the data management and processing. PoSeID-on reserves the right to modify at any time these Terms and Conditions. In this case, PoSeID-on will notify the modified Terms to the User by sending an email to the address provided when registered, or on the first access to the site and will ask the User to accept the new modified version of the Terms. 

###### __Registration and execution of Contract__'

For the access and use of PoSeID-on’s services the Users shall register on the Platform by completing the appropriate registration form. To ensure registration, the Users shall previously read and accept these Terms and Conditions. In case the User does not intend to accept these Terms, he should not continue with the registration, access and use of the Platform. The access to the Platform and reserved areas, for which registration is required, is free. These Terms will be effective upon the Users’ acceptance and will govern the relations with PoSeID-on for the all execution of the contract.

---

###### __Terms Duration and Termination for withdrawal__

These Terms is for an indefinite period, subject to the possible withdrawal of either party, operated to pursuant to the following Section. The Users or PoSeID-on might at any time ask for the cancellation withdrawing the acceptance of the Terms with either party. PoSeID-on may withdraw notifying the User by the email address provided at registration and this withdrawal will be effective in _______ (___) days after notification; the Users can exercise the above mentioned right by sending notice by email to info@poseidon-h2020.eu and the withdrawal shall be effective in _______ days (___) after notification.

---

###### __Services and using the Platform__

By accessing to PoSeID-on Platform, the Users will be able to use the Privacy Enhancing Dashboard (PED) for personal data protection, aimed to safeguard the rights of data subjects as well as support public and private organizations in data management and processing while ensuring the GDPR compliance. The PED allows the data subjects to maintain the control of their own data by having a concise, transparent, intelligible and ease access, as well as tracking, control and management of their Personality Identifiable Information (PII) processed by public or private organizations, acting as data controllers and/or data providers. In this way the Users will be able to make conscious decisions about who can process their own data by enabling, restricting or revoking permissions in accordance to the data minimization principle, as well as to be alerted in case of privacy exposure. Paralleling, PoSeID-on Platform will also represent a valuable instrument supporting private and public organizations processing personal data ensuring compliance of their data protection and control procedures with the EU regulations, as required by the GDPR.  
The Privacy Enhancing Dashboard (PED) contains a series of security mechanisms in order to protect the Personally Identifiable Information (PII) and the communication between the data subjects and data controllers/processors (private and public organizations). The Permissioned Blockchain will through the Risk Management Module and the Personal Data Analyzer check the legitimacy of data processing and the data exchanges between the different parties by alerting the data subjects in case of aberration leading to the breach of their fundamental rights and freedom; indeed, PoSeID-on Platform through the Risk Management Module will advise data subjects  for any kind of privacy threats or risk exposure by sending warnings or alarms, while, the Personal Data Analyzer will alarm the data subjects to identify anomalous patterns of transactions carried out by historical data analysis.
Paralleling for supporting the public and private organizations, the Privacy Enhanced Dashboard will enforce their data protection and control procedures by the implementation of the following tools:  
    • the Permissioned Blockchain and Smart Contracts will enable public and private organizations to have a contextual guarantee of accountability, transparency and compliance with the EU data protection Regulation. Indeed, through the implementation of Smart Contracts on the Permissioned Blockchain, PoSeID-on will ensure the quality of personal data collected that will be managed directly by the data subject in order to be accurate and up to date. The system has been conceived in a way that, the specific Smart Contract will contain the reference to just the User’s personal data necessary for that specific transaction, in compliance with the data minimization principle;
    • cloud, access management according to eIDAS (electronic Identification, Authentication and trust Services, the EU regulation on electronic identification and trust services for electronic transactions in the internal market) and privacy management.
Moreover, PoSeID-on Platform can also be used as a set of open source tools and toolkits, that can be separated deployed by the Users according to their specific needs. This will mitigate the TCO (Total Cost Ownership) for public agencies and SMEs facing budget constraints. All the available tools can be in this way utilized:
    • the Privacy Enhanced Dashboard as an ICT integrated prototype also provided with an innovative web-based dashboard for data subjects with a user-friendly interface. It can be used by organizations that want to integrate their procedures with a GDPR compliant tool; 
    • open source components or API, as interoperable ICT components that can be integrated in any public or private ICT architecture. In this way, the organizations can integrate every single component of the Privacy Enhanced Dashboard in their own systems achieving a high technological development and competitiveness;
    • cloud-based Privacy Enhanced Dashboard as Service (PEDaaS). This service can be used by the organizations that do not have their own blockchain and/or cloud or they don’t want to afford, for any reason, the cost of managing the GDPR compliant tools. In this way, they can access the PoSeID-on cloud service and use the Privacy Enhanced Dashboard to monitor and control the data processing.    

---

###### __Obligations and Responsibilities of the User__

The use of the available material and services provided by the Platform is under the exclusive responsibility, control and discretion of the User. The User acknowledges and agrees that, it is his responsibility to use the services provided by the Platform in compliance with any rule of these Terms and Conditions and according to the regulations in force and agrees not to hold PoSeID-on responsible for any damage or loss that may be caused by such activities. 
In case of breach of this Terms and in the event the behavior of the User does not comply with these Terms or with the policy of PoSeID-on, PoSeID-on reserves the right to limit, suspend, interrupt the account of the User without prior notice or, in the most serious cases, forbid the access to the Platform and its services and take legal action against him.    
The User is responsible for the confidentially, truthfulness and completeness of his login credentials (username and password) while registering and to check its updating and accuracy for the entire duration of use of the Platform’s services. PoSeID-on cannot be held responsible for the truthfulness of the information nor for any mistake deriving from inaccuracies and/or incorrect of the inserted data by the User of the Platform. The User shall prevent the use of his login credentials by third parties and report immediately to PoSeID-on any loss of his exclusive control over this information, taking note of the fact that, in the absence of such notification, he is liable for any action caused by third parties. 
The User undertakes not to use the Platform to transmit, distribute or provide third parties any type of content that in any way can:  
    • harass, threaten or abuse other Users of the Platform;
    • encourage other Users to breach the Platform’s rules or other legal rules;
    • impersonate members of the PoSeID-on’s Platform staff or other Users using a similar identity or any other tool or device;
    • perform spam actions such us carry out publications that may damage who receives them or to carry out commercial, advertising or illegal proposals;
    • induce or incite to criminal, degrading, defamatory, violent of discriminatory actions due to sex, age, illness, creed etc.;
    • encourage involvement in actions that are dangerous, risky may damage people’s health or the psychological status;
    • send viruses that could damage or interrupt the normal use of the network, the system or the computer and the PoSeID-on’s Platform tools;
    • breach material protected by third parties copyright, or by PoSeID-on’s copyright without a prior written authorization;
    • cause difficulties in the correct provision of the Platform’ services.

---

###### __Warranties and Limitations of liability of the Platform__

PoSeID-on undertakes to use the personal data provided by the Users during the registration in full compliance with the current EU Regulation on the personal data protection. PoSeID-on will take provisional and protective measures to ensure the confidentiality and integrity of the Users’ personal data processed during the entire use of the Platform’s services, but PoSeID-on will not be responsible for any abusive use or for the loss, alteration or dissemination of information, due to causes not attributable to it. 
PoSeID-on will make every reasonable effort to ensure continued access to the Users without interruption to the contents and services, but may not, under any circumstances, be held responsible if one or more of the services are temporarily inaccessible. By way of example, PoSeID-on is not liable for the interruption of services as a result of force majeure or malfunction of services due to the incorrect functioning of telephone lines, electricity and global and/or national networks. PoSeID-on cannot be held responsible for direct or indirect damages suffered by the Users as a result of the use of the Platform. PoSeID-on undertakes to keep all the contents on the Platform duly updated and without mistakes or omissions but cannot always guarantee it. Consequently, PoSeID-on is not responsible for any mistake or omission that may appear in the contents of the Platform, or that may be due to its inaccuracies, completeness or updating. Once the mistakes or the omissions have been highlighted, PoSeID-on undertakes to correct them as quickly as possible.
PoSeID-on is not responsible and cannot monitor or verify the contents of other websites that can be accessed through the hypertext links found on the Platform, as it cannot recommend their use under any circumstances. PoSeID-on assumes responsibility not to publish on the Platform material that does not meet the requirements established by these Terms and undertakes to remove, as soon as possible, any information or false and misleading content that could damage the Users. The Platform allows the Users the option to report any information or content that does not meet with these Terms and that is therefore inappropriate by writing to the following email address info@poseidon-h2020.eu. In any case, PoSeID-on reserves the right to remove the contents that it deems to be non-compliant with its own policies.

---

###### __Intellectual Property Rights__

Except where otherwise stated, all the material available on the Platform such as images, photographs, videos, texts are owned by the legitimate copyright holder PoSeID-On and protected by the Italian and European copyright laws. Therefore, it is forbidden to reproduce any material of the Platform, even in part, without a prior PoSeID-on’s written authorization.
Any text and material taken from other sources or any trademark of third parties utilized on the Platform, for which prior authorization for the publication has been given, are also owned by the legitimate copyright holders and protected by the copyright law.
Any use of the material in violation of the above-mentioned limits will be objective of legal action by PoSeID-On.

---

###### __Applicable Law and Jurisdiction Disputes__

This Agreement is governed by the Italian Law. 
The eventual nullity and/or invalidity, in whole or in part, of one or more items, does not affect the other Articles contained in this contract which, consequently, shall therefore be regarded as fully valid and effective.
Disputes which may arise between the parties relating to the interpretation, execution and validity of this contract shall be to the exclusive jurisdiction of the Court of Rome. 

---

###### __Privacy__

The Information provided by the User represent his personal data according to the EU Regulation 2016/679 on the Personal Data Protection.          
With reference to the Regulation, PoSeID-on is the Data Controller of the personal data provided by the User in relation to all the aspects connected to the use of the Platform. 
PoSeID-on has provided a policy privacy on the processing of personal data, available at the address _____________________/privacy-policy, which must be accepted by the User before proceeding with the registration and the use of the Platform.

---

###### __Approval of the specific clauses__

clauses: art. 2 - duration of contract and termination for withdrawal, art. 4 – obligations and responsibilities of the User; art. 5  – warranties and limitations of liability of the Platform; art. 7 - applicable law and jurisdiction disputes.
