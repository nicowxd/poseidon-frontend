import DashboardIcon from "@material-ui/icons/Dashboard";
import HelpIcon from "@material-ui/icons/Help";
import MessageIcon from "@material-ui/icons/Sms";
import ProcessorsIcon from "@material-ui/icons/LibraryBooks";
import PrivacyIcon from "@material-ui/icons/SupervisedUserCircle";
import SettingsIcon from "@material-ui/icons/Settings";
import TermsIcon from "@material-ui/icons/NewReleases";

//App Constants
export const MSG_POLLING_TIME = 10000 // time is set in ms
export const MSG_PROTECTED_PII = ["employment_contract", 
  "salary_information",
  "social_security_number",
  "bank_details",
  "license_plate_number",
  "national_id_number",
  "passport_number",
 ] 

// App routes
export const ROOT_PATH = "/";
export const LOGIN_PATH = "/auth/";
export const HELP_PATH = "/help";
export const MESSAGES_PATH = "/messages";
export const ORGANISATIONS_PATH = "/organisations";
export const PRIVACY_PATH = "/privacy";
export const SETTINGS_PATH = "/settings";
export const TERMS_PATH = "/terms";

// API
export const API_ROUTE = "/api";
export const API_AUTH = `${API_ROUTE}/auth`;
export const API_DATA_PROCESSORS = `${API_ROUTE}/data-processors`;
export const API_PERMISSIONS = `${API_ROUTE}/permissions`;
export const API_KEYS_AUTHENTICATE = `${API_ROUTE}/keys/authenticate`;
export const API_KEYS_USER = `${API_ROUTE}/keys/me`;
export const API_MESSAGES = `${API_ROUTE}/messages`;
export const API_DATA_PROCESSORS_CORRELATE = `${API_DATA_PROCESSORS}/correlate`;
export const API_GET_DP_LIST = `${API_ROUTE}/access`

// Component logic
export const NOTIFICATION_TIMEOUT = 3500;

// Nav items
export const APP_MENU_ITEMS = [
  {
    icon: DashboardIcon,
    messageId: "general.label.dashboard",
    path: ROOT_PATH
  },
  {
    icon: MessageIcon,
    messageId: "general.label.messages",
    path: MESSAGES_PATH
  },
  {
    icon: ProcessorsIcon,
    messageId: "general.label.dataProcessors",
    path: ORGANISATIONS_PATH
  },
  {
    icon: HelpIcon,
    messageId: "general.label.help",
    path: HELP_PATH
  }
];
export const USER_MENU_ITEMS = [
  {
    icon: TermsIcon,
    messageId: "general.label.termsAndConditions",
    path: TERMS_PATH
  },
  {
    icon: PrivacyIcon,
    messageId: "general.label.privacyPolicy",
    path: PRIVACY_PATH
  },
  {
    icon: SettingsIcon,
    messageId: "general.label.settings",
    path: SETTINGS_PATH
  }
];
