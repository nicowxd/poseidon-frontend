import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Router, Switch } from 'react-router-dom';
import { createIntl, createIntlCache, RawIntlProvider } from 'react-intl'
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import notificationsMiddleware from 'middlewares/notifications';
import reducers from 'reducers';
import sagas from 'sagas';

import localeData from 'i18n/index';
import i18n from 'utils/i18n';

import { history } from 'utils/history';
import { ScrollToTop } from 'utils/scroll';

import { ROOT_PATH } from './constants.js';
import App from './App';
import * as serviceWorker from './serviceWorker';

import './index.module.scss';

export const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  combineReducers(reducers),
  composeWithDevTools({})(
    applyMiddleware(sagaMiddleware, notificationsMiddleware)
  )
);

// Run sagas
sagaMiddleware.run(sagas);

// Optional, but prevents memory leaks
const cache = createIntlCache();

const intl = createIntl({
  locale: 'en',
  messages: i18n(localeData)
}, cache);

ReactDOM.render(
  <Provider store={store}>
    <RawIntlProvider value={intl}>
      <Router history={history}>
        <ScrollToTop>
          <Switch>
            <Route path={ROOT_PATH} component={App}/>
          </Switch>
        </ScrollToTop>
      </Router>
    </RawIntlProvider>
  </Provider>,
  document.getElementById('root')
);


// TODO only for demo
if (module.hot && window.location.pathname === "/fake-processor") {
  module.hot.accept();
  const NewApp = require('./FakeDataProcesorForDemo/index.js').default;
  ReactDOM.render(<NewApp />,document.getElementById('root'));
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
