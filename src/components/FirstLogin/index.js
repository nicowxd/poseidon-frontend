import React from 'react';
import InfoIcon from '@material-ui/icons/Info';
import { useIntl } from "react-intl";
import TextField from 'components/TextField';


import styles from './index.module.scss';

const FirstLogin = ({ firstLogin, firstRecovery }) => {

  const intl = useIntl();

  return (
    <div>
    {firstRecovery ? (
      <div className={styles.card}>
        <div className={styles.header}>
          <InfoIcon 
            color = 'error'
            fontSize = 'large'
          ></InfoIcon>
          <p>
            {intl.formatMessage({ id: "general.label.recovery.firstTimeWarning"})}
          </p>
        </div>
        <p>
          {intl.formatMessage({ id: "general.label.recovery.firstTime"})}
        </p>
        <TextField
          disabled
          multiline
          value = {firstRecovery}
        />
      </div>
    ) : ""}

    </div>
  );
};

export default FirstLogin;
