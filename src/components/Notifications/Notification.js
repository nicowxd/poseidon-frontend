import React from 'react';
import FormattedMessage from 'react-intl/lib/components/message';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

import styles from './index.module.scss';

const Notification = ({ error, message, messageId, onClose }) => (
  <Snackbar className={styles.wrapper} open>
    <SnackbarContent
      classes={{ root: styles[error ? 'error' : 'info'] }}
      message={
        <span className={styles.message}>
          { error
            ? <ErrorIcon className={styles.icon} />
            : <InfoIcon className={styles.icon} />
          }
          {message || (messageId && <FormattedMessage id={messageId} />)}
        </span>
      }
      action={
        <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
          <CloseIcon />
        </IconButton>
      }
    />
  </Snackbar>
);

export default Notification;
