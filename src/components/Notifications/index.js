import React from 'react';

import Notification from './Notification';

const Notifications = ({ className, notifications = [], onClose }) => (
  <div className={className}>
    { notifications.map(({ id, error, message, messageId }) =>
      <Notification
        key={id}
        error={error}
        message={message}
        messageId={messageId}
        onClose={() => onClose(id)}
      />
    ) }
  </div>
);

export default Notifications;
