import React, { useState, Fragment } from "react";
import classNames from "classnames";
import ExpandIcon from "@material-ui/icons/ExpandMore";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

import UserMenu from "components/UserMenu";

import styles from "./index.module.scss";

const UserAvatar = ({ name, className, showName, showMenu, onClick }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const handleMenuOpen = evt => {
    setAnchorEl(evt.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <Fragment>
      <div
        className={classNames(styles.wrapper, className)}
        onClick={showMenu === true ? handleMenuOpen : onClick}
      >
        <AccountCircleIcon
          fontSize="large"
          className="customUserIcon"
        ></AccountCircleIcon>
        {/* <Avatar classes={{ root: styles.avatar }}>{initials}</Avatar> */}
        {/* {showName === true && <span className={styles.name}>{name}</span>} */}
        {showMenu === true && <ExpandIcon classes={{ root: styles.icon }} />}
      </div>
      {showMenu === true && (
        <UserMenu anchorEl={anchorEl} onClose={handleMenuClose} />
      )}
    </Fragment>
  );
};

export default UserAvatar;
