import React, {useState} from 'react';
import NewFlag from 'components/NewFlag'
import WarningFlag from 'components/WarningFlag'
import styles from './index.module.scss';

const PageList = ({ items = [], filters, messageIds, template: ListItem, onItemClick }) => {

  const setTagContent = (index) => {
    if(document.location.pathname.includes("/messages")){
      let tag = "new warning";
      if (items[index].params.pii_types.includes('poseidon_transactions')){
        tag = "important";
      }
      return tag;
    }
  }

  const isImportant = (index) => {
    if(document.location.pathname.includes("/messages")){
      let important = false
      if (items[index].params.pii_types && items[index].params.pii_types.includes('poseidon_transactions')){
        important = true;
      }
      return important;
    }
  }

  return(
    <div>
      {filters}
      {items.map((item, index) =>
        <div key={`item_${index}`} className={styles.listItem}>
          { messageIds && messageIds[item.id] === false ? ( item.warning === true ? <WarningFlag isNew={setTagContent(index)} index={index} important={isImportant(index)}/> : <NewFlag isNew={"new"} index={index}/>): ""}
          <ListItem {...item} onClick={onItemClick} isImportant={isImportant(index)} />
        </div>
      )}
    </div>
  )
}

export default PageList;
