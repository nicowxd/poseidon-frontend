import React, { useEffect } from 'react';

import Auth from 'components/Auth';
import LoadingSpinner from 'components/LoadingSpinner';

import styles from './index.module.scss';

const AuthWrapper = (props) => {
  const {
    authProvider,
    blockchain,
    children,
    getStatus,
    invalidPassphrase,
    isSubmitting,
    submitPassphrase,
    resetValidation,
    isRegistered,
  } = props;

  useEffect(() => {
    getStatus();
  }, [authProvider, getStatus]);


  return !authProvider
    ? <LoadingSpinner  className={styles.loader} />
    : !blockchain
      ? <Auth
        invalid={invalidPassphrase}
        isSubmitting={isSubmitting}
        onSubmit={submitPassphrase}
        onResetValidation={resetValidation}
        isRegistered={isRegistered}
      />
      : React.cloneElement(children);
};
export default AuthWrapper;
