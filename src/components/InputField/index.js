import React from "react";
import { TextField } from '@material-ui/core';
import styles from "./index.module.scss";


class InputField extends React.Component {
    constructor(props) {
      super(props);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.updateFields = this.updateFields.bind(this);

      this.state = {
          value: this.props.default,
          id: this.props.id,
          typingTimeout: 0
      }
    }

    componentDidMount() {
        this.updateFields();
    }

    componentDidUpdate(prevProps) {
        if(this.props.default !== prevProps.default){
            this.updateFields();
        }
    }

    updateFields() {
        this.setState({value: this.props.default})
    }
  
    handleSubmit(event) {

        if(this.state.typingTimeout){
            clearTimeout(this.state.typingTimeout);
        }
        this.setState({
            value: event.target.value,
            typingTimeout: setTimeout((value = this.state.value, id=this.state.id, certificate=this.props.certificate) => {
                this.props.editPiiField(value, id, certificate)
              }, 1500)
        })
    }
  
    render() {
      return (
        <TextField
        value ={this.state.value}
        placeholder = "Data processor has no value for this field"
        onChange ={this.handleSubmit}
        className={styles.field}
        >
        </TextField>
      );
    }
  }
  

export default InputField

