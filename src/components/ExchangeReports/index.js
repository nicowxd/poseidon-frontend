import React, { useEffect } from 'react';
import FormattedMessage from 'react-intl/lib/components/message';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';

import { ContentCard, CardLogo, CardContent } from 'components/ContentCard';
import PageHeading from 'components/PageHeading';
import UserAvatar from 'containers/UserAvatar';

import styles from './index.module.scss';

const MIN_DESKTOP_WIDTH = 560;

const Report = ({ description, organisationLogo }) => (
  <div className={styles.report}>
    <CardLogo src={organisationLogo} alt="logo" />
    <SwapHorizIcon classes={{ root: styles.swapIcon }} />
    <UserAvatar />
    <div className={styles.desc}>
      <div className={styles.name}>{description}</div>
      <div className={styles.date}>10.37 | 07.04.2019</div>
    </div>
    {window.innerWidth < MIN_DESKTOP_WIDTH ? "" : <div className={styles.bntNew}>New</div>}
    {window.innerWidth < MIN_DESKTOP_WIDTH ? "" : <MoreVertIcon classes={{ root: styles.moreIcon }} />}
  </div>


);

const ExchangeReports = ({ className, reports = [], onGetReports }) => {
  useEffect(() => {
    onGetReports();
  }, [onGetReports]);
  return (
    <div className={className}>
      <PageHeading>
        <FormattedMessage id="component.exchangeReports.title" />
      </PageHeading>
      <ContentCard>
        <CardContent>
          {
            reports.map(({ description, image_url }, index) =>
              <Report
                key={`report_${index}`}
                description={description}
                organisationLogo={image_url}
              />
            )
          }
        </CardContent>
      </ContentCard>
    </div>
  );
};

export default ExchangeReports;
