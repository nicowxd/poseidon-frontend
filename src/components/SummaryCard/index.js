import React, { useEffect } from 'react';
import FormattedMessage from 'react-intl/lib/components/message';
import CallMadeIcon from '@material-ui/icons/CallMade';
import CallReceivedIcon from '@material-ui/icons/CallReceived';
// import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import PageHeading from 'components/PageHeading';

import styles from './index.module.scss';

const SummaryCard = ({
  className,
  requests,
  organisations,
  permissions,
  getSummary
}) => {
  useEffect(() => {
    getSummary()
  }, [getSummary]);

  return (
    <div className={className}>
      <PageHeading>
        <FormattedMessage id="general.label.summary"/>
      </PageHeading>
      <div className={styles.card}>
        <div className={styles.summary}>
          <CallMadeIcon classes={{ root: styles.arrowUpIcon }} />
          <span className={styles.count}>{organisations}</span>
          <span className={styles.type}>
          <FormattedMessage id="component.summaryCard.dataProcessors"/>
        </span>
        </div>
        <div className={styles.summary}>
          <CallReceivedIcon classes={{ root: styles.arrowDownIcon }} />
          <span className={styles.count}>{permissions}</span>
          <span className={styles.type}>
          <FormattedMessage id="component.summaryCard.permissions"/>
        </span>
        </div>
      </div>
    </div>
  );
};

export default SummaryCard;
