import React, {useState} from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import Terms from "components/TacContent"
import styles from './index.module.scss';
import { useIntl } from 'react-intl';

const RegisterForm = ({onCheck}) => {

  const intl = useIntl();

  const [checkboxDisabled, setCheckboxDisabled] = useState(true);

  const handleScroll = (e) => {
    const bottom = Math.round(e.target.scrollHeight - e.target.scrollTop) === e.target.clientHeight;
    if (bottom) {setCheckboxDisabled(false) }
  }

  return(
    <div>
      <Terms 
      register = {true}
      handleScroll = {handleScroll}
      />
      <div className={styles.checkboxWrapper}>
        <div>
          <p>{intl.formatMessage({ id: "general.label.acceptTerms" })}</p>
          <Checkbox 
              color="primary"
              onChange={onCheck}
              inputProps={{ 'aria-label': 'disabled checked checkbox'}} />
        </div>
      </div>
    </div>
  )
};

export default RegisterForm;
