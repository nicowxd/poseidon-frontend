import React, {Fragment, useEffect} from 'react';
import {useDispatch} from "react-redux";
import { Link } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';

import PageList from 'components/PageList';
import PageTitle from 'components/PageTitle';
import { ContentCard, CardHeader, CardContent, CardLogo, CardAlert, CardActions } from 'components/ContentCard';
import ListFilters from 'components/ListFilters';
import { ORGANISATIONS_PATH } from 'constants.js';

import styles from "./index.module.scss";
import {GET_PII_TYPES} from "../../reducers/organisation";
import {RESET_PII} from "../../reducers/dataFields";

const noop = () => {};
const OrganisationListItem = ({ name, description, image_url, certificate }) => {
  const intl = useIntl();
  const dispatch = useDispatch();

  return (
    <Link className={styles.link} to={`${ORGANISATIONS_PATH}/${certificate}`}>
      <ContentCard className={styles.card}>
        <CardHeader
          avatar={<CardLogo src={image_url} alt={name} />}
          title={description}
          subheader="Last interaction | 10.37 | 07.04.2019"
        />
        <CardContent className={styles.content}>
          <div className={styles.content}>
            <div className={styles.desc}>
              <span className={styles.field}>
                <FormattedMessage id="component.organisationsList.listItem.field1" />
              </span>
              <span className={styles.count}>0</span>
            </div>
            <div className={styles.desc}>
              <span className={styles.field}>
                <FormattedMessage id="component.organisationsList.listItem.field2" />
              </span>
              <span className={styles.count}>3</span>
            </div>
            <div className={styles.desc}>
              <span className={styles.field}>
                <FormattedMessage id="component.organisationsList.listItem.field3" />
              </span>
              <span className={styles.count}>2</span>
            </div>
          </div>
        </CardContent>
        <CardActions className={styles.actions}>
          <CardAlert
            text={intl.formatMessage({ id: "general.label.newDataRequest" })}
          />
          <span className={styles.cta}><FormattedMessage id="general.label.settings" /> >></span>
        </CardActions>
      </ContentCard>
    </Link>
  );
};

const OrganisationsList = ({ organisations, getOrganisations }) => {
  // Fetch data
  useEffect(() => {
    getOrganisations();
  }, [getOrganisations]);

  return (
    <Fragment>
      <PageTitle>
        <FormattedMessage id="component.organisationsList.title" />
        <span className={styles.btn}> {organisations.length} </span>
      </PageTitle>
      <PageList
        items={organisations}
        filters={<ListFilters showDisplay onSelectFilter={noop}/>}
        template={OrganisationListItem}
      />
    </Fragment>
  );
};

export default OrganisationsList;
