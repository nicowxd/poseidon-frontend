import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {TextareaAutosize} from "@material-ui/core";
import logo from 'assets/images/poseidon_logo_md.png';
import Button from 'components/Button';
import TextField from 'components/TextField';
import PasswordField from 'components/TextField/PasswordField';
import RegisterForm from 'components/RegisterForm'


import styles from './index.module.scss';

const Auth = ({ invalid, isSubmitting, onResetValidation, onSubmit, isRegistered }) => {
  const intl = useIntl();
  const [passphrase, setPassphrase] = useState('');
  const [recoveryPassphrase, setRecoveryPassphrase] = useState('');
  const [confirmation, setConfirmation] = useState('');
  const [register, setRegister] = useState(isRegistered);
  const [visibility, setVisibility] = useState(false);
  const [recovery, setRecovery] = useState(false);
  const [checked, setChecked] = useState(true);

  const onChangeConfirmation = evt => {
    setConfirmation(evt.target.value);
    setRecoveryPassphrase(evt.target.value);
  };

  const onChangePassphrase = evt => {
    onResetValidation();
    setPassphrase(evt.target.value);
  };

  const onSubmitPassphrase = () => {
    onSubmit(passphrase, register, recovery, recoveryPassphrase);
  };

  const onKeyPressedEnter = (event: React.KeyboardEvent<HTMLDivElement>): void => {
    if (event.key === "Enter") {
      event.preventDefault();
      onSubmit(passphrase, register, recovery, recoveryPassphrase);
    }
  };

  const onToggleRecovery = () => {
    onResetValidation();
    setConfirmation('');
    setPassphrase('');
    setRecovery(!recovery);
    setRegister(false);
  };

  const onCheck = () =>{
    setChecked(!checked)
  }

  return (
    <Dialog
      open
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <form className={styles.content}>
          <img className={styles.logo} src={logo} alt="Poseidon Logo" />
          <PasswordField
            className={styles.input}
            fullWidth
            disabled={isSubmitting}
            error={invalid}
            label={recovery ? intl.formatMessage({id: "general.label.new.passphrase"}) : intl.formatMessage({ id: "general.label.passphrase" })}
            ctaAriaLabel={intl.formatMessage({ id: "auth.component.passphrase.reader" })}
            value={passphrase}
            onChange={onChangePassphrase}
            onKeyPress={onKeyPressedEnter}
            onToggleVisibility={setVisibility}
          />

          { register && <div>
            <TextField
              className={styles.input}
              disabled={isSubmitting}
              error={invalid}
              fullWidth
              label={intl.formatMessage({ id: "auth.component.passphrase.confirm" })}
              type={visibility ? "text" : "password"}
              value={confirmation}
              onChange={onChangeConfirmation}
            />
            <RegisterForm 
              onCheck={onCheck}
            />
          </div>
          }

          { recovery && <TextareaAutosize
            rows={6}
            colsMax={35}
            className={styles.textarea}
            disabled={isSubmitting}
            error={invalid}
            fullWidth
            label={intl.formatMessage({ id: "general.label.recovery.passphrase" })}
            value={confirmation}
            onChange={onChangeConfirmation}
          />
          }
          <Button
            className={styles.submitButton}
            variant="contained"
            color="primary"
            fullWidth
            size="large"
            type="button"
            showLoader={isSubmitting}
            onClick={onSubmitPassphrase}
            disabled={ !recovery && !register ? false : checked}
          >
            { register
              ? 
              intl.formatMessage({ id: "general.label.register"}) : "" }

            { recovery
              ? intl.formatMessage({ id: "general.label.recover"}) : "" }

            { !recovery && !register
              ? intl.formatMessage({ id: "general.label.authenticate" }) : "" }

          </Button>
          { !register && !recovery
          ?<Button
            className={styles.register}
            color="primary"
            disabled={isSubmitting}
            onClick={ onToggleRecovery }
          >
            {intl.formatMessage({ id: "general.label.recover" })}
          </Button>
          :""
          }
          { recovery ? <Button
            className={styles.register}
            color="primary"
            disabled={isSubmitting}
            onClick={ onToggleRecovery }
          > {intl.formatMessage({ id: "general.label.back" })}
          </Button>: ""
          }
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default Auth;
