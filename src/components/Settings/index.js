import React, {useState} from "react";
import {useIntl} from "react-intl";
import {useDispatch} from "react-redux";
import {CHANGE_PASSWORD} from "../../reducers/settings";
import PasswordField from "../TextField/PasswordField";
import Button from "../Button";
import {TextareaAutosize} from "@material-ui/core";
import styles from "./index.module.scss";
import PageTitle from 'components/PageTitle';
import { FormattedMessage } from 'react-intl';
import { CardActions } from 'components/ContentCard';


const Settings = ({
  recoveryPassphrase
}) => {

  const intl = useIntl();
  const [passphrase, setPassphrase] = useState('');
  const [newPassphrase, setNewPassphrase] = useState('');
  const [visibility, setVisibility] = useState(false);
  const [frontendError, setFrontendError] = useState('');
  const obj = {};
  const dispatch = useDispatch();

  const onChangePassphrase = evt => {
    setPassphrase(evt.target.value);
    obj.passphrase = passphrase;

  };

  const onChangeNewPassphrase = evt => {
    setNewPassphrase(evt.target.value);
  };

  const onValidate = () => {

    if (passphrase === newPassphrase) {
      setFrontendError(intl.formatMessage({id: "general.label.settings.frontend.error"}));
      return false;
    } else if (passphrase === "" || newPassphrase === "") {
      setFrontendError(intl.formatMessage({id: "general.label.settings.frontend.required.error"}));
      return false;
    } else {
      setFrontendError("");
      onSubmit();
    }
  };

  const onSubmit = () => {
    dispatch({
      type: CHANGE_PASSWORD,
      payload: {passphrase: passphrase, newPassphrase: newPassphrase}
    })
  };

    return (
      <div className={styles.flex}>
      <PageTitle style>
        <FormattedMessage id="settings.title" />
      </PageTitle>
        <CardActions>
          <PageTitle className={styles.subtitle}>
            <FormattedMessage id="settings.changePassword" />
          </PageTitle>
        </CardActions>
        <div className={styles.center}>
          <form className={styles.content}>
            <PasswordField
              className={styles.input}
              fullWidth
              label={intl.formatMessage({id: "general.label.passphrase"})}
              ctaAriaLabel={intl.formatMessage({id: "auth.component.passphrase.reader"})}
              type={visibility ? "text" : "password"}
              onChange={onChangePassphrase}
              onToggleVisibility={setVisibility}
            />

            <PasswordField
              className={styles.input}
              fullWidth
              label={intl.formatMessage({id: "general.label.new.passphrase"})}
              onChange={onChangeNewPassphrase}
              type={visibility ? "text" : "password"}
              onToggleVisibility={setVisibility}
            />

            {recoveryPassphrase ?
              <TextareaAutosize
                rows={6}
                cols={35}
                className={styles.textarea}
                placeholder={intl.formatMessage({id: "general.label.recover.password.message"})}
                label={intl.formatMessage({id: "general.label.recover.password.message"})}
                value={recoveryPassphrase}
              /> : ""}

            <Button
              variant="contained"
              color="default"
              size="medium"
              className={styles.btn}
              onClick={onValidate
              //   () => dispatch({
              //   type: CHANGE_PASSWORD,
              //   payload: {passphrase: passphrase, newPassphrase: newPassphrase}
              // })
              }
            >
              {intl.formatMessage({id: "general.label.changePassword"})}
            </Button>
          </form>
            <p className={styles.error}>{frontendError}</p>
        </div>
      </div>
    );
};

export default Settings;
