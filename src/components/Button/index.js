import React from 'react';
import MuiButton from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

const Button = ({ children, showLoader = false, ...props}) => (
  <MuiButton
    disabled={showLoader === true}
    {...props}
  >
    { showLoader
      ? <CircularProgress color="inherit" size="1.625rem" /> // TODO: sizing should be stored in scss
      : children
    }
  </MuiButton>
);

export default Button;
