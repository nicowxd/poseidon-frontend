import React from 'react';
import classNames from 'classnames';

import styles from './index.module.scss';

const PageContent = ({ children, className }) => (
  <div className={classNames(styles.wrapper, className)}>
    {children}
  </div>
);

export default PageContent;
