import * as React from 'react';
import classNames from 'classnames';

import Sidebar from 'components/Sidebar';
import Topbar from 'components/Topbar';

import { isDesktopScreen, withScreenSize } from 'utils/screen';

import styles from './index.module.scss';

const noop = () => {};

class Layout extends React.Component {
  state = {
    sidebarOpen: false
  };

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    const { screen: { width } } = this.props;
    const { screen: { width: nextWidth } } = nextProps;
    const { sidebarOpen } = this.state;

    // Block rendering only for screen width changes
    return width !== nextWidth
      ? isDesktopScreen(width) !== isDesktopScreen(nextWidth)
        ? (sidebarOpen && this.onToggleSidebar()) || true
        : false
      : true
  }

  onToggleSidebar = () => {
    const { sidebarOpen } = this.state;

    this.setState(() => ({ sidebarOpen: !sidebarOpen }));
  };

  render() {
    const { children, screen: { width } } = this.props;
    const { sidebarOpen } = this.state;
    const isDesktop = isDesktopScreen(width);

    return (
      <div className={styles.wrapper}>
        <Topbar
          className={styles.topbar}
          isMobile={!isDesktop}
          onToggleSidebar={(!isDesktop && this.onToggleSidebar) || noop}
        />
        <div
          className={classNames(
            styles.sidebar,
            {
              [styles.desktop]: isDesktop === true,
              [styles.open]: sidebarOpen || isDesktop
            }
          )}
        >
          <Sidebar
            showUserMenu={!isDesktop}
            onClick={(!isDesktop && this.onToggleSidebar) || noop}
          />
        </div>
        <main
          id = "content"
          className={classNames(
            styles.pageContent,
            {
              [styles.desktop]: isDesktop === true,
              [styles.hidden]: sidebarOpen
            }
          )}
        >
          {children}
        </main>
      </div>
    );
  }
}

export default withScreenSize(Layout);
