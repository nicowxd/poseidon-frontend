import React, {Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import FormattedMessage from 'react-intl/lib/components/message';
import { ContentCard, CardHeader } from 'components/ContentCard';
import PageList from 'components/PageList';
import PageTitle from 'components/PageTitle';
import Moment from 'react-moment';
import styles from './index.module.scss';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';


import {
  MESSAGES_GET_MESSAGES,
  GET_CURRENT_PAGE
} from 'reducers/messages';


const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const MessagePreview = ({ warning, active, read, id, timestamp, identifier, sender, onClick, isImportant }) => {
  const onHandleClick = () => {
    onClick(id);
  };

/*eslint-disable */
  const organisations = useSelector(state => state.organisations.processors)

  const getDataProcessorName = (cert) => {
    for (let i in organisations){
      if (organisations[i].certificate === cert) {
        return organisations[i].description;
      }
    }
    return ""
  }
/*eslint-enable */

  return (
    <div>
      {warning === true ?
      <ContentCard
      className={ isImportant
        ? classNames(
          {[styles.activeImportant]: active === true},
          {[styles.action]: typeof onClick === 'function'}
        )
        : classNames(
        {[styles.activeWarning]: active === true},
        {[styles.action]: typeof onClick === 'function'}
      )}
      onClick={onHandleClick}
    >
      <CardHeader
        title={
        <span className={styles.header}>
            <Moment format="YYYY/MM/DD HH:mm:ss" className={styles.date} unix>{timestamp}</Moment>
            {["privacy_notification", "risk_notification", "warning_notification"].includes(identifier)
            ? <h3>{getDataProcessorName(sender)} {identifier.replace(/_/g, " ")}</h3>
            : <h3>{getDataProcessorName(sender)} wants to {identifier.replace(/_/g, " ")}</h3>
            }
          </span>
        }
      />
      { isImportant ?
        <div className={styles.importantTag}>
          <h4>Important</h4>
        </div>
      :
        <div className={styles.warningTag}>
          <h4>Warning</h4>
        </div>
      }
    </ContentCard>
      :
      <ContentCard
      className={classNames(
        {[styles.active]: active === true},
        {[styles.action]: typeof onClick === 'function'}
      )}
      onClick={onHandleClick}
    >
      <CardHeader
        title={
        <span className={styles.header}>
            <Moment format="YYYY/MM/DD HH:mm:ss" className={styles.date} unix>{timestamp}</Moment>
            {["privacy_notification", "risk_notification", "warning_notification"].includes(identifier)
            ? <h3>{getDataProcessorName(sender)} {identifier.replace(/_/g, " ")}</h3>
            : <h3>{getDataProcessorName(sender)} wants to {identifier.replace(/_/g, " ")}</h3>
            }
          </span>
        }
      subheader= "message content"
      />
    </ContentCard>
    }
    </div>
  );
};

const MessagesList = ({ messages, getMessages, getOrganisations, messageIds, onOpenMessage, nrOfMessages, getNrOfPages, pages}) => {
  // Fetch data
  useEffect(() => {
    getMessages();
  }, [getMessages]);

  useEffect(() => {
    getOrganisations();
  }, [getOrganisations]);

  useEffect(() => {
    getNrOfPages();
  }, [getNrOfPages]);

  const dispatch = useDispatch()
  const classes = useStyles();

  const handleChange = (event, page) => {
    dispatch({type: MESSAGES_GET_MESSAGES, page:page, itemsPerPage: 15})
    dispatch({type: GET_CURRENT_PAGE, page: event.target.innerText})
  };

  return (
    <Fragment>
      <PageTitle>
        <FormattedMessage id="component.messagesList.title" />
        <span className={styles.btn}> {nrOfMessages} </span>
      </PageTitle>
      <PageList
        items={messages}
        template={MessagePreview}
        onItemClick={onOpenMessage}
        messageIds={messageIds}
        pages = {pages}
      />
      <div className={classes.root}>
        <Pagination color="primary" count={pages} onChange={handleChange} />
      </div>
    </Fragment>
  );
};

export default MessagesList;
