import React, { useEffect, useState, useRef } from 'react';
import styles from './index.module.scss';
import { MSG_POLLING_TIME } from 'constants.js'
import { useDispatch } from 'react-redux';

import {
  MESSAGES_GET_MESSAGES,
} from 'reducers/messages';



const MessageMenuCount = (props) => {
  const {
    getMessages,
    getNrOfMessages,
    nrOfMessages,
    currentPage
  } = props;

  const dispatch = useDispatch()
  const [page, setPage] = useState(currentPage);

  const prevPageRef = useRef();
  useEffect(() => {
    prevPageRef.current = page;
    if(prevPageRef !== prevPageRef.current){
      setPage(currentPage)
    }
  });

  
  const getMessageCount = () => dispatch({type: MESSAGES_GET_MESSAGES, page:page, itemsPerPage: 15})

  useEffect(() => {
    const interval = setInterval(() => {
      getMessageCount();
      }, MSG_POLLING_TIME);
      return () => clearInterval(interval);
    }
  , [getMessageCount]);

  return (
    <span className={styles.btn}> {nrOfMessages} </span>
  );
};

export default MessageMenuCount;
