import React from 'react';
import styles from './index.module.scss';

const NewFlag = ({ isNew, index }) => (
    <span id={'newTag' + index} className={styles.newMsg} >
      {isNew}
    </span>
);

export default NewFlag;

