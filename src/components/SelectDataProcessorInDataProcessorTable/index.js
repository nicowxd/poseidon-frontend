import React, {useState} from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import styles from "./index.module.scss";
import Button from "components/Button";
import { useDispatch } from 'react-redux';
import { 
  API_GET_DP_LIST, 
  API_DATA_PROCESSORS 
} from 'constants.js'
import {
  UPDATE_ACCESS_PII,
} from 'reducers/messageContent'

class GrantAccess extends React.Component {

  constructor(props){
    super(props)
    this._isMounted = false;
    this.state = { dataProcessorsCertificate: [], listOfDataProcessors: [], toggle: false, defaultValue: ""}
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchDataProcessorsWithField();
    this.fetchAllOrganizations();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  fetchDataProcessorsWithField = () =>{
    fetch(`${API_GET_DP_LIST}/${this.props.piiField}`)
    .then(response => response.json())
    .then(data => {
      this._isMounted && this.setState({ dataProcessorsCertificate: Object.keys(data)})
    })
  }

  fetchAllOrganizations = () => {
    fetch(`${API_DATA_PROCESSORS}`)
      .then(response => response.json())
      .then(data =>
        {
          let listOrganisations = []
          for(let i in data.processors){
            let newOrg = {
              name: data.processors[i].description,
              certificate: data.processors[i].certificate
            }
            listOrganisations.push(newOrg)
          }
          this._isMounted &&  this.setState({ listOfDataProcessors: listOrganisations})
        }
      )
  }

  findDataProcessorName =  (cert) => {
    if(this.state.listOfDataProcessors.length > 0 ){
      let dataProcessor = this.state.listOfDataProcessors.find(o => 
        o.certificate === cert)
      return (dataProcessor.name)
      }
  }

  render(){
  return(
    <FormControl variant="outlined" className={styles.formControl}>
        <Select
          labelId="selectDP"
          value = {this.state.dataProcessor}
          className={styles.selectOptions}
          defaultValue={this.props.dataProcessor}
          onChange={this.props.handleSelectInput}
        >
          {
            this.state.dataProcessorsCertificate.map((option, index) => (
              (this.state.dataProcessorsCertificate[index] !== this.props.certificate) ? 
              (
              <MenuItem 
                key={index}
                value={option}
              >{this.findDataProcessorName(option)}
              </MenuItem>) : null
           ))
          }
        </Select>
      </FormControl>
  )}
}
 


const SelectDataProcessor = ({field, dataProcessor, certificate}) => {

  const dispatch = useDispatch();
  const [selectValueInput, setSelectValueInput] = useState ([]);

  const updateValue = () => {
    dispatch({ type: UPDATE_ACCESS_PII, field: field, sharePii: selectValueInput, dpCertificate: certificate})
  }

  const handleSelect = (e) => {
    let selectArray = selectValueInput;
    let newSelectItem = {
        field: e.target.name,
        value: e.target.value
    }

    if(selectArray.find(name => name.field === newSelectItem.field)){
        let index = selectArray.findIndex(pii => pii.field === e.target.name)
        selectArray[index] = newSelectItem
    } else {
        selectArray.push(newSelectItem)
    }
    setSelectValueInput(selectArray)
    updateValue()
  }

  return ( 
    <GrantAccess
      piiField={field}
      updateValue={updateValue}
      handleSelectInput= {handleSelect}
      certificate = {certificate}
      dataProcessor= {dataProcessor}
    ></GrantAccess>)
}

export default SelectDataProcessor
