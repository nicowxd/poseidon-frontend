import React, { useEffect } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import styles from "./index.module.scss";
import TextField from '@material-ui/core/TextField';
import FormattedMessage from 'react-intl/lib/components/message';
import Switch from '@material-ui/core/Switch';
import LoadingSpinner from 'components/LoadingSpinner';
import ClassicTextInput from 'components/ClassicTextInput';
import { API_GET_DP_LIST, API_DATA_PROCESSORS } from 'constants.js';

class GrantAccess extends React.Component {

  constructor(props){
    super(props)

    this.handleChangePiiValue = this.handleChangePiiValue.bind(this);

    this._isMounted = false;


    this.state = {
      initialValue:"",
      currentValue: "",
      currentlId:"",
      piiValue: "", 
      dataProcessorsCertificate: [], 
      listOfDataProcessors: [], 
      toggle: false, 
      isLoaded: false, 
      typingTimeout: 0 
    }
  }

  componentDidMount() {

    fetch(`${API_GET_DP_LIST}/${this.props.piiField}`)
      .then(response => response.json())
      .then(data => 
        this.setState({ dataProcessorsCertificate: Object.keys(data)})
      )

    fetch(`${API_DATA_PROCESSORS}`)
      .then(response => response.json())
      .then(data =>
        {
          let listOrganisations = []
          for(let i in data.processors){
            let newOrg = {
              name: data.processors[i].description,
              certificate: data.processors[i].certificate
            }
            listOrganisations.push(newOrg)
          }
          this.setState({ listOfDataProcessors: listOrganisations})
        }
      )

      fetch(`${API_DATA_PROCESSORS}/${this.props.certificate}/${this.props.piiField}`)
        .then(response => response.json())
        .then(data => {
          this.setState({ piiValue: data.value})
        })
  }

  componentDidUpdate(prevProps) {
    if (this.props.piiFieldValues !== prevProps.piiFieldValues) {
      this.setState({piiValues: this.props.piiFieldValues, isLoaded: true, initialValue: this.props.piiValue})
      this.props.handleInitialState(this.props.piiField, this.state.piiValue)
    }
  }

  handleToggle = () => {
    this.state.toggle === false ? this.setState({ toggle: true}) : this.setState({ toggle: false})
  }

  findDataProcessorName =  (cert) => {
    if(this.state.listOfDataProcessors.length > 0 ){
      let dataProcessor = this.state.listOfDataProcessors.find(o => 
        o.certificate === cert)
      return (dataProcessor.name)
      }
  }

  handleChangePiiValue(event) {

    if(this.state.typingTimeout){
        clearTimeout(this.state.typingTimeout);
    }
    
    this.setState({
      currentValue: event.target.value,
      currentlId: event.target.name,
      typingTimeout: setTimeout(() => {
          this.props.handleFieldInputUpdate(this.state.currentValue, this.state.currentlId)
        }, 1500)
    })

}


  render(){
  return<div>
  { this.state.isLoaded 
    ?(this.state.dataProcessorsCertificate.length !== 0 && this.state.dataProcessorsCertificate[0] !== this.props.certificate)  ? (
      <div>
      Switch the toggle to change input data manually
      <Switch
        onChange={this.handleToggle}
        color="primary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />{
      }
      {this.state.toggle === false ? (
        <div className={styles.containerSelectOption}>
          <p>Select the data processor that will provide this information</p>
          <FormControl variant="outlined" className={styles.formControl}>
            <InputLabel id="selectDP">{this.props.piiField.replace("_"," ")}</InputLabel>
            <Select
              labelId="selectDP"
              value={this.props.processor}
              onChange={this.props.handleSelectInput}
              label={this.props.piiField.replace("_"," ")}
              className={styles.selectOptions}
              name={this.props.piiField}
            >
              {
                this.state.dataProcessorsCertificate.map((option, index) => (
                  (this.state.dataProcessorsCertificate[index] !== this.props.certificate) ? 
                  (
                  <MenuItem 
                    key={index}
                    value={option}
                  >{this.findDataProcessorName(option)}
                  </MenuItem>) : null
              ))
              }
            </Select>
          </FormControl>
        </div>
      ) : (
        <FormControl variant="outlined" className={styles.formControl}>
          <p>
            <FormattedMessage id="general.label.messageFieldUnfilled" />
          </p>
          <TextField 
            id="piiInput" 
            label={this.props.piiField.replace("_"," ")} 
            className={styles.inputField} 
            onChange={this.props.handleFieldInput}
            name={this.props.piiField}
          />
        </FormControl>
        )}  
      </div>
    ) : (
      <FormControl variant="outlined" className={styles.formControl}>
        <p>
          {
            this.state.piiValue === "" 
            ? <FormattedMessage id="general.label.messageFieldUnfilled" />
            : <FormattedMessage id="general.label.messageFieldFilled" />
          }
        </p>{
        }
        { 
          // <TextField 
          //   defaultValue={this.state.piiValue}
          //   id="piiInput"
          //   label={this.props.piiField.replace("_"," ")}
          //   onChange={this.state.piiValue !== undefined ? this.handleChangePiiValue : this.props.handleFieldInput}
          //   name={this.props.piiField}
          // />
          <ClassicTextInput
            type="text" 
            defaultValue={this.state.piiValue}
            id="piiInput"
            label={this.props.piiField.replace("_"," ")}
            onChange={this.state.piiValue !== undefined ? this.handleChangePiiValue : this.props.handleFieldInput}
            name={this.props.piiField}
        />
        }
      </FormControl>
    )
    : (this.props.index === 0 
      ? <LoadingSpinner  className={styles.customLoader} />
      : null)
    }
  </div>
  }
}
 


const SelectDataProcessor = ( { index, certificate, piiField, onGetReports,  handleFieldInput, handleSelectInput, handleFieldInputUpdate, getDataProcessorList, handleInitialState,  piiFieldValues =[]} ) => {

  useEffect(() => {
    onGetReports();
  }, [onGetReports]);

  const listOrganisations = [];

  return ( 
    <GrantAccess
      getDataProcessorList={getDataProcessorList} 
      piiField={piiField}
      listOrganisations = {listOrganisations}
      handleSelectInput={handleSelectInput}
      handleFieldInput = {handleFieldInput}
      handleFieldInputUpdate = {handleFieldInputUpdate}
      certificate = {certificate}
      handleInitialState={handleInitialState}
      piiFieldValues= {piiFieldValues}
      index={index}
    ></GrantAccess>)
}

export default SelectDataProcessor