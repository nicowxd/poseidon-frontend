import React from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import FormattedMessage from 'react-intl/lib/components/message';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import LogoutIcon from '@material-ui/icons/ExitToApp';

import { logout } from 'reducers/auth';
import { USER_MENU_ITEMS } from 'constants.js';

import styles from './index.module.scss';

const UserMenu = ({ anchorEl, onClose }) => {
  const dispatch = useDispatch();
  const onClick = () => dispatch(logout());

  return (
    <Menu
      anchorEl={anchorEl}
      id="user-account-menu"
      open={Boolean(anchorEl)}
      onClose={onClose}
    >
      {USER_MENU_ITEMS.map(({icon: Icon, messageId, path}) => (
        <div key={path}>
          <NavLink className={styles.link} to={path}>
            <MenuItem classes={{root: styles.item}} key={path} onClick={onClose}>
              <Icon classes={{root: styles.icon}}/>
              <FormattedMessage id={messageId}/>
            </MenuItem>
          </NavLink>
        </div>
      ))}
      <MenuItem classes={{root: styles.item}} onClick={onClick}>
        <span className={styles.link}>
          <LogoutIcon classes={{root: styles.icon}}/>
          <FormattedMessage id="general.label.logout"/>
        </span>
      </MenuItem>
    </Menu>
  );
};

export default UserMenu;
