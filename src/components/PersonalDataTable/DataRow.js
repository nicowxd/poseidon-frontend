import React, { useState, useEffect } from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/Button";
import InfoIcon from "components/PersonalDataTable/InfoIcon";
import styles from "./index.module.scss";
import { useIntl } from "react-intl";
import { useDispatch } from "react-redux";
import { EDIT_PII } from "reducers/dataFields";
import InputField from "components/InputField"
import SelectDataProcessor from "components/SelectDataProcessorInDataProcessorTable"
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Moment from 'react-moment';


const Cell = props => <TableCell classes={{ root: styles.cell }} {...props} />;

const DataRow = ({
  file,
  expires,
  shared,
  pii,
  info,
  onRevoke,
  field,
  dataProcessor,
  certificate,
}) => { 

  const intl = useIntl();
  const [check, setCheck] = useState(shared);
  const [inputType, setInputType] = useState(false)
  const [checked, setChecked] = useState(false);
  const dispatch = useDispatch();

  const buttonHandler = () => {
    onRevoke(field, certificate);
  };

  useEffect(() => {
    dataProcessor ? setInputType(true) : setInputType(false)
  }, [dataProcessor]);

  const revokePermissionButton = () => {
    setCheck(!check);

    if (!check) {
      intl.formatMessage({
        id: "component.personalDataTable.revoke.permission"
      });
    } else {
      intl.formatMessage({ id: "component.personalDataTable.revoke.grant" });
    }
  };

  const editPiiField = (value, id, certificate) => {
    dispatch({
      type: EDIT_PII,
      payload: { pii: value, field: id, certificate: certificate}
    });
  };


  const toggleChecked = () => {
    setChecked((prev) => !prev);
    revokePermissionButton()
  };

  const handleChangeInput = () => {
    setInputType(!inputType)
  }


  return (
    <TableRow classes={{ root: styles.row }}>
      <Cell align="center" className={styles.info}>
        <span data-tip={info}>
          <InfoIcon />{intl.formatMessage({ id: `pii.${field}` })}
        </span>
      </Cell>
      <Cell>
        <div>
          <div className={styles.inputSwitchContainer}> 
          <Switch
            checked={inputType}
            onChange={handleChangeInput}
            color="primary"
            name="checkedB"
            inputProps={{ 'aria-label': 'primary checkbox' }}
          />
          <div>
            {inputType === false ? <p>
              Toggle the switch to select data from existing processors
            </p> : <p>
              Toggle the switch to input data directly
            </p>}
          </div>
          </div>
          { inputType === false ? (
            <InputField 
              id={field} 
              default={pii} 
              certificate={certificate} 
              editPiiField={editPiiField}
            />
          ) : (
            <SelectDataProcessor
              field={field}
              dataProcessor={dataProcessor}
              certificate={certificate}
            />
          )}
        </div>
      </Cell>
      <Cell align="center">
        <span className={styles.file}>{file}</span>
      </Cell>
      <Cell align="center">
        <Moment format="YYYY/MM/DD" className={styles.date} unix>{expires}</Moment>
      </Cell>
      <Cell align="center">
          <FormControlLabel
            control={
              <Switch 
              color="primary"
              checked={checked}
              onChange={toggleChecked} />
            }
          />
      </Cell>
      <Cell>
        <span>
          <Button
            className={styles.gradientBtn}
            variant="outlined"
            size="small"
            type="button"
            onClick={buttonHandler}
          >
            {!check
              ? intl.formatMessage({
                  id: "component.personalDataTable.revoke.permission"
                })
              : intl.formatMessage({
                  id: "component.personalDataTable.revoke.grant"
                })}
          </Button>
        </span>
      </Cell>
    </TableRow>
  );
};

export default DataRow;
