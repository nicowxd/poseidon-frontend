import React from 'react';
import Icon from '@material-ui/icons/Info';
import classNames from 'classnames';

import styles from './index.module.scss';

const InfoIcon = ({ className, ...props }) =>
  <Icon
    classes={{ root: classNames(styles.icon, className) }}
    {...props}
  />;

export default InfoIcon;
