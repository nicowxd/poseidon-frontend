import React, { Fragment, memo, useCallback, useState } from "react";
import FormattedMessage from "react-intl/lib/components/message";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "components/Button";
import { Link } from "react-router-dom";
import { ORGANISATIONS_PATH } from "constants.js";
import { CardContent, ContentCard } from "components/ContentCard";
import ListFilters from "components/ListFilters";
import DataRow from "./DataRow";
import styles from "./index.module.scss";
import ReactTooltip from "react-tooltip";
import { useIntl } from "react-intl";


const Filters = memo(({ items, ...props }) => {
  const filters = items.reduce((acc, item) => {
    acc.indexOf(item.status) === -1 && acc.push(item.status);

    return acc;
  }, []);
  return <ListFilters filters={filters} {...props} allItems={items} />;
});

const HeadCell = props => (
  <TableCell classes={{ root: styles.cell, head: styles.head }} {...props} />
);

const PersonalDataTable = ({organisationCertificate, fields=[], permissions =[], onRevoke, revokeAllPermissions, certificate }) => {

  const intl = useIntl();
  const fieldsOfDataProcessor = [];
  const count = permissions.length;
  const [filter, setFiler] = useState();
  const onSetFilter = useCallback(filter => setFiler(filter), []);

  for( let i in permissions){
    fieldsOfDataProcessor.push(permissions[i])
  }

  const dataFields = !filter
    ? fields
    : // Filter list
      fields.reduce((acc, field) => {
        if (field.status === filter) {
          acc.push(field);
        }
        return acc;
      }, []);

  const revokeAll = () => {
    revokeAllPermissions(organisationCertificate)
  }

  return (
    <Fragment>
      <Button
        variant="contained"
        size="medium"
        className={styles.removePermissionButton}
        onClick={revokeAll}
        >
        {intl.formatMessage({ id: "general.label.revokeAllPermissions" })}
      </Button>
      <Filters
        className={styles.filters}
        items={fields}
        active={filter}
        filteredItems={dataFields.length}
        onSelectFilter={onSetFilter}
      />
      <ContentCard>
        <CardContent>
          <ReactTooltip place="left" type="success" effect="solid" />
          <Table>
            <TableHead>
              <TableRow
                classes={{
                  root: styles.row,
                  head: styles.head,
                }}
              >
                <HeadCell>
                  <FormattedMessage id="component.personalDataTable.info" />
                </HeadCell>
                <HeadCell>
                  <FormattedMessage
                    id="component.personalDataTable.dataList"
                    values={{count}}
                  />
                </HeadCell>
                {/* <HeadCell>
                  <FormattedMessage id="component.personalDataTable.file" />
                </HeadCell> */}
                <HeadCell>
                  <FormattedMessage id="component.personalDataTable.expire" />
                </HeadCell>
                <HeadCell>
                  <FormattedMessage id="component.personalDataTable.select" />
                </HeadCell>
                <HeadCell />
              </TableRow>
            </TableHead>
              <TableBody>
                {fieldsOfDataProcessor.map((field, index) => (
                  <DataRow
                    className={styles.tableRow}
                    key={field.name}
                    info = {field.display}
                    data = {1}
                    expires = {field.deadline}
                    pii = {field.value}
                    onRevoke={onRevoke}
                    field={field.name}
                    dataProcessor={field.data_processor}
                    certificate={organisationCertificate}
                    ownerCertificate={certificate}
                  />
                ))}
              </TableBody>
          </Table>
          {/* <Link>
            <Button
              className={styles.button}
              variant="contained"
              color="primary"
              onclick="expandInfo()"
            >
              Show more
            </Button>
          </Link> */}
        </CardContent>
      </ContentCard>
      {window.innerWidth < 787 ? (
        <Link to={ORGANISATIONS_PATH}>
          <Button className={styles.button} variant="contained" color="primary">
            Back
          </Button>
        </Link>
      ) : null}
    </Fragment>
  );
};

export default PersonalDataTable;
