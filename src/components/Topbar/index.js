import React from 'react';
import classNames from 'classnames';

import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import logo from 'assets/images/poseidon_logo_sm.png';
import UserAvatar from 'containers/UserAvatar';

import styles from './index.module.scss';
import {history} from 'utils/history';
import {Link} from "react-router-dom";
import FormattedMessage from 'react-intl/lib/components/message';

const MIN_DESKTOP_WIDTH = 560;
const backButton =
  <Link to="/organisations" >
    <div className={styles.goBack}>
      <KeyboardBackspaceIcon className={styles.arrowIcon}/> <FormattedMessage id="general.go.back" />
    </div>
  </Link>;

const Topbar = ({ className, isMobile = true, onToggleSidebar }) => (
  <div className={classNames(styles.wrapper, className)}>
    <img className={styles.logo} src={logo} alt="Poseidon Logo" />
    {history.location.pathname.startsWith("/organisations/") && window.innerWidth > MIN_DESKTOP_WIDTH ? backButton : ""}
    <UserAvatar
      showName={!isMobile}
      showMenu={!isMobile}
      onClick={onToggleSidebar}
    />
  </div>
);

export default Topbar;
