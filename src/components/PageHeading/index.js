import React from 'react';

import styles from './index.module.scss';

const PageHeading = ({ children }) => <h2 className={styles.heading}>{children}</h2>;

export default PageHeading;
