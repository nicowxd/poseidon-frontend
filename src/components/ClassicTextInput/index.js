import React, { useEffect } from 'react';

import styles from './index.module.scss';

const ClassicTextInput = (props) => {
  const {
    type,
    defaultValue,
    label,
    onChange,
    name
  } = props;


  return (
      <div className={styles.wrapper}>
          <input
            id={name}
            type={type}
            defaultValue={defaultValue}
            placeholder=" "
            onChange = {onChange}
            name= {name}
          />
          <label htmlFor={name}>{label}</label>
      </div>
  )
};
export default ClassicTextInput;
