import React from 'react';
import styles from './index.module.scss';

const WarningFlag = ({ isNew, index, important }) => (
    <span id={'newTag' + index} className={important ? styles.newImportant : styles.newWarning} >
      {isNew}
    </span>
);

export default WarningFlag;

