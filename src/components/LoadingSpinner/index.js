import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const LoadingSpinner = ({ className }) =>
  <div className={className}>
    <CircularProgress />
  </div>;

export default LoadingSpinner;
