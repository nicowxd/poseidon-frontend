import React from 'react';

import styles from './index.module.scss';

const LinearGradientDef = () =>
  <svg
    style={{ width:0, height:0, position: 'absolute' }}
    aria-hidden="true"
    focusable="false"
  >
    <linearGradient id="linearGradientHorizontal" x2="1" y2="1">
      <stop offset="0%" stopColor={styles.startColor} />
      <stop offset="100%" stopColor={styles.stopColor} />
    </linearGradient>
  </svg>;

  export default LinearGradientDef;
