import React, { useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import classNames from 'classnames';
import { ContentCard, CardAlert, CardLogo, CardHeader, CardContent, CardActions } from 'components/ContentCard';
import PageTitle from 'components/PageTitle';
import StarRating from 'components/Rating';
import PersonalDataTable from 'containers/PersonalDataTable';
import styles from './index.module.scss';
import {GET_PII_TYPES} from "../../reducers/organisation";
import {RESET_PII} from "../../reducers/dataFields";
import {useDispatch} from "react-redux";

const OrganisationView = ({
  certificate,
  description,
  image_url,
  name,
  fields,
  onClearData,
  onGetData,
  onRevoke,
  getFields,
  revokeAllPermissions,
  contact_details
}) => {

  const intl = useIntl();
  const dispatch = useDispatch();

  useEffect(() => {
    onGetData(certificate);

    // cleanupl
    return () => { onClearData(); };
  }, [certificate, onClearData, onGetData]);

    useEffect(() => {
      getFields(certificate);
    }, [getFields]);

  return (
    <div className={styles.grid}>
      <PageTitle className={styles.title}>
        <FormattedMessage
          id="component.organisationView.title"
          values={{ organisation: description }}
        />
      </PageTitle>
      <div className={classNames(styles.section, styles.main)}>
        <span className={styles.sectionName}>
          <FormattedMessage id="component.organisationOverview.generalInfo" />
        </span>
        <ContentCard>
          <CardHeader
            avatar={<CardLogo src={image_url} alt={name} />}
            title={description}
            subheader="Last interaction | 10.37 | 07.04.2019"
          />
          <div className={styles.dataProcessorInfo}>
            <CardHeader
              title ={intl.formatMessage({ id: "general.label.contactDetails" })}
              subheader= {contact_details ? (contact_details.split('\n')).map((data, index) => {
                return <div key={index}>{data}</div>
              }): ""}
            >
            </CardHeader>
            <StarRating rating={3}></StarRating>
          </div>
          <CardActions className={styles.cardAlert}>
              <CardAlert
                text={intl.formatMessage({ id: "general.label.newDataRequest" })}
              />
          </CardActions>

          </ContentCard>

      </div>
      <div className={styles.section}>
        <span className={styles.sectionName}>
          <FormattedMessage id="component.organisationOverview.latestActivity" />
        </span>
        <ContentCard>
          <CardContent>
            <CardHeader
              className={styles.activity}
              title="Personal information update"
              subheader="10.37 | 07.04.2019"
            />
            <CardHeader
              className={styles.activity}
              title="Credit card renewal"
              subheader="10.35 | 07.04.2019"
            />
          </CardContent>
        </ContentCard>
      </div>
      <div className={classNames(styles.section, styles.full)}>
        <PersonalDataTable
          organisationFields = {fields}
          onRevoke={onRevoke}
          organisationCertificate = {certificate}
          revokeAllPermissions = {revokeAllPermissions}
        />
      </div>
    </div>
  );
};

export default OrganisationView;
