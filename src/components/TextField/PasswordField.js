import React, { useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import TextField from './index';

const PasswordField = ({ ctaAriaLabel, onToggleVisibility, ...props}) => {
  const [show, setShow] = useState(false);
  const onToggle = () => {
    setShow(!show);
    onToggleVisibility(!show);
  };

  return (
    <TextField
      {...props}
      type={show ? "text" : "password" }
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              edge="end"
              aria-label={ctaAriaLabel}
              onClick={onToggle}
            >
              {show ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        )
      }}
    />
  )
};

export default PasswordField;
