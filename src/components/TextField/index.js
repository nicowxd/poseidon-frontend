import React from 'react';
import MuiTextField from '@material-ui/core/TextField';

const TextField = ({ className, ...props }) => (
  <MuiTextField
    classes={{ root: className }}
    variant="outlined"
    {...props}
  />
);

export default TextField;
