import React from 'react';
import { useIntl } from 'react-intl';
import classNames from 'classnames';
import FilterListIcon from '@material-ui/icons/FilterList';
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewModuleIcon from '@material-ui/icons/ViewModule';

import styles from './index.module.scss';

const FilterItem = ({ active = false, filter, onSelect, piiNumber }) => (

  <span className={classNames(styles.filter, { [styles.active]: active === true })} onClick={onSelect}>
    {filter === 'By default' ? <span>{filter} </span> : <span>{filter} {'(' + piiNumber + ')'}</span>}

    <FilterListIcon className={styles.icon} />
  </span>
);

/*eslint-disable */
const ListFilters = ({ active, className, filters = [], showDisplay = false, onSelectFilter, allItems }) => {
  const intl = useIntl();
  let activeItems = 0;
  let requestedItems = 0;

  if (allItems) {
    allItems.map((item) => {
      if (item.status === "active") {
        activeItems += 1;
      }
      if (item.status === "requested") {
        requestedItems += 1;
      }
    })
  }
/*eslint-enable */

  return (
    <div className={classNames(styles.filters, className)}>
      <FilterItem
        active={!active}
        filter={intl.formatMessage({ id: "component.listFilters.default" })}
        onSelect={() => onSelectFilter()}
      />

      {filters.map(filter => (
        <FilterItem
          key={`filter_${filter}`}
          active={filter === active}
          filter={filter}
          onSelect={() => onSelectFilter(filter)}
          piiNumber={filter === "active" ? activeItems : requestedItems}
        />
      ))}
      {showDisplay === true && (
        <span className={styles.display}>
        <ViewListIcon className={classNames(styles.icon, styles.active)} />
        <ViewModuleIcon className={styles.icon} />
      </span>
      )}
    </div>
  );
};

export default ListFilters;
