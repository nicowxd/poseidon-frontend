import React from 'react';
import { useSelector } from 'react-redux';
import styles from './index.module.scss';
import Moment from 'react-moment';
import MessageContent from "../MessageContent"

const MessageCard = ({ id, identifier, message, sender, timestamp, params, redirect_url}) => {

  const organisations = useSelector(state => state.organisations.processors)
  const userCertificate = useSelector(state => state.auth.certificate)
  const piiArray = []

/*eslint-disable */
  const getDataProcessorName = (cert) => {
    for (let i in organisations){
      if (organisations[i].certificate === cert) {
        return organisations[i].description;
      }
    }
    return ""
  }

  if(params && params.pii_types !== undefined){
    let newArr = params.pii_types.split(",")
      for ( let i in newArr){
      piiArray.push(newArr[i].replace(" ", ""))
    }
  }

/*eslint-enable */
  const body = {
    "sender": getDataProcessorName(sender),
    "identifier": identifier,
    "params" : params,
    "pii_array" : piiArray,
    "certificate" : sender
  };
  
  const user ={
    "certificate": userCertificate
  }

  const title = () => {
    if (identifier !== undefined){
      return (identifier.replace(/_/g, " "))
    }
  }

  return(
    <section className={styles.card}>
        <span className={styles.from}>Message received from {getDataProcessorName(sender)}</span>
        <div className={styles.header}>
          <h2 className={styles.subject}>{title()}</h2>
          <Moment format="YYYY/MM/DD HH:mm:ss" className={styles.date} unix>{timestamp}</Moment>
        </div>
        <MessageContent 
          body={body}
          user={user}
          >
        </MessageContent>
      </section>
      );
};

export default MessageCard;
