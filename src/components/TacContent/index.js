import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';
import termsPath from 'assets/termsAndConditions/tacContent.md';
import styles from './index.module.scss';

class Terms extends Component {
  constructor(props) {
    super(props)
    this.state = { terms: null }
  }

  componentDidMount() {
    fetch(termsPath).then((response) => response.text()).then((text) => {
      this.setState({ terms: text })
    })
  }
  render() {
    return (
      <div 
        className={(this.props.register === true ? styles.parentRegister : styles.parent)}
        onScroll={this.props.handleScroll}
        >
        <div className={styles.content}>
            <ReactMarkdown source={this.state.terms} />
        </div>
      </div>
    )
  }
  
}
export default Terms;