import React from 'react';
import classNames from 'classnames';

import styles from './index.module.scss';

const PageTitle = ({ children, className }) =>
  <h1 className={classNames(styles.title, className)}>
    {children}
  </h1>;

export default PageTitle;
