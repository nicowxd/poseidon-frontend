import React from 'react';
import FormattedMessage from 'react-intl/lib/components/message';
import {useDispatch} from 'react-redux';
import {NavLink} from 'react-router-dom';
import LogoutIcon from '@material-ui/icons/ExitToApp';

import {logout} from 'reducers/auth';
import MessageMenuCount from '../../containers/MessageMenuCount';
import {APP_MENU_ITEMS, USER_MENU_ITEMS} from 'constants.js';
import styles from './index.module.scss';

const SidebarNav = ({ icon: Icon, messageId, path }) => (
  <NavLink
    className={styles.navItem}
    activeClassName={styles.active}
    to={path}
  >
    <Icon classes={{ root: styles.navIcon }} />
    <FormattedMessage id={messageId} />
    { path === "/messages" ? <MessageMenuCount />: "" }
  </NavLink>
);

const Sidebar = ({ showUserMenu, onClick }) => {
  const dispatch = useDispatch();
  const onLogout = () => dispatch(logout());

  return (
    <div className={styles.wrapper} onClick={onClick}>
      {APP_MENU_ITEMS.map((link, index) =>
        <SidebarNav key={`appNav_${index}`} {...link} />)
      }
      {showUserMenu === true && USER_MENU_ITEMS.map((link, index) =>
        <SidebarNav key={`userNav_${index}`} {...link} />)
      }
      {showUserMenu === true &&
        <div className={styles.navItem} onClick={onLogout}>
          <LogoutIcon classes={{ root: styles.navIcon }} />
          <FormattedMessage id="general.label.logout" />
        </div>
      }
    </div>
  );
};

export default Sidebar;
