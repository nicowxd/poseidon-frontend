import React, { Fragment } from 'react';
import FormattedMessage from 'react-intl/lib/components/message';
import ChevronRightRoundedIcon from '@material-ui/icons/ChevronRightRounded';
import TOC from 'utils/normalizers/tableOfContent';

import PageTitle from 'components/PageTitle';
import TocList from 'components/TocList';


import styles from './index.module.scss';


const onClickHandle = (chapter) => {
  let headers;

  const index = parseInt(chapter.substr(chapter.lastIndexOf(' '))) - 1;
  if (!headers) {
    headers = document.getElementsByTagName("h6");
  }
  let elmnt = headers[index];
  elmnt.scrollIntoView({behavior: "smooth"});
}

const TocPreview = ({ chapter, subject, art }) => (
    <div className={styles.tocItems} onClick={() => onClickHandle(chapter)}> 
      <div>
        <p className={styles.chapter}>{chapter}</p>
        <p className={styles.subject}>{subject}</p>
        <p className={styles.art}>{art}</p>
      </div>
        <ChevronRightRoundedIcon />
    </div>
);

const TableOfContent = () => {

  const toc = TOC;

  return (
    <Fragment>
      <PageTitle>
        <FormattedMessage id="component.tableOfContent.title" />
      </PageTitle>
        <div className={styles.tocWrapper}>
          <TocList
            items={toc}
            template={TocPreview}
          />
        </div>
    </Fragment>
  );
};

export default TableOfContent;
