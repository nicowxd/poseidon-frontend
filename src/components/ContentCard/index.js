import React from 'react';
import classNames from 'classnames';
import MuiCard from '@material-ui/core/Card';
import MuiCardActions from '@material-ui/core/CardActions';
import MuiCardContent from '@material-ui/core/CardContent';
import MuiCardHeader from '@material-ui/core/CardHeader';
import ErrorIcon from '@material-ui/icons/Error';

import styles from './index.module.scss';

export const CardAlert = ({ text }) =>
  <div className={styles.alert}>
    <ErrorIcon classes={{ root: styles.icon }} />
    {text && <span>{text}</span>}
  </div>;

export const CardContent = ({ className, ...props }) =>
  <MuiCardContent
    {...props}
    classes={{ root: className }}
  />;

export const CardActions = ({ className, ...props }) =>
  <MuiCardActions
    {...props}
    classes={{ root: classNames(styles.actions, className) }}
  />;

export const CardLogo = ({ src, alt }) =>
  <span className={styles.logo}>
    <img src={src} alt={alt} />
  </span>;

export const CardHeader = ({ className, ...props }) =>
  <MuiCardHeader
    {...props}
    classes={{
      root: classNames(styles.header, className),
      title: styles.title,
      subheader: styles.subheader
    }}
  />;

export const ContentCard = ({ className, ...props }) =>
  <MuiCard
    {...props}
    classes={{ root: classNames(styles.card, className) }}
  />;

