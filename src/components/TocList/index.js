import React from 'react';

import styles from './index.module.scss';

const TocList = ({ items = [], filters, template: ListItem }) => (
  <div>
    {filters}
    {items.map((item, index) =>
      <div key={`item_${index}`} className={styles.listItem}>
        <ListItem {...item} />
      </div>
    )}
  </div>
);

export default TocList;
