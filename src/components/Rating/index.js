import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import styles from './index.module.scss';



const StarRating = ({rating}) => {

  return (
    <div className={styles.ratingBar}>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Rating name="read-only" value={rating} readOnly />
        <p>Rating score of: {rating}</p>
      </Box>
    </div>
  );
}

export default StarRating