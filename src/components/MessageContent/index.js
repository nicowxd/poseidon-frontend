import React, { Fragment, useState } from 'react';
import Button from 'components/Button';
import styles from './index.module.scss';
import Moment from 'react-moment';
import FormattedMessage from 'react-intl/lib/components/message';
import { useSelector, useDispatch } from 'react-redux';
import SelectDataProcessor from "containers/SelectDataProcessor"
import InputLabel from '@material-ui/core/InputLabel';

import {
    EDIT_PII
} from "reducers/dataFields";

import {
    GRANT_ACCESS_PII,
} from 'reducers/messageContent'

import {
    GET_FIELDS,
} from 'reducers/dataFields'


const renderMessage = (body, user, sendPii, getPiiValues, handleSelect, handledTextField, checkProcessor, handleFieldInputUpdate, handleInitialState) =>{

    /*eslint-disable */
    const organisations = useSelector(state => state.organisations.processors)

    const getDataProcessorName = (cert) => {
    for (let i in organisations){
        if (organisations[i].certificate === cert) {
        return organisations[i].description;
        }
    }
    return ""
    }
    /*eslint-enable */

    switch (body.identifier) {
        case 'correlate':
            return <div>
            {body.sender} wants to correlate data with you. To accept please click the confirm button below.
            <Button
                className={styles.button}
                variant="contained"
                color="primary"
                fullWidth
                size="large"
                type="button">correlate
            </Button>
            </div>

        case 'request_permission':
            return <div>
            {body.sender} needs the following data from you: <b>{(body.params.pii_types.replace(/,/g, ', ')).replace(/_/g, " ")}</b> because {body.params.reason} { body.params.until === "0" ? "" : "expiring date:" } {
                body.params.until === "0" ? 'indefinitely' : <Moment format="YYYY/MM/DD HH:mm:ss" unix>{body.params.until}</Moment>}
            <br></br>
            <div className={styles.piiSelectorContainer}>
            {body.pii_array.map((pii, index) => (
                <div key={pii} className={styles.piiSelectorLine}>
                    {pii === "poseidon_transactions"
                        ? <InputLabel>
                            <FormattedMessage id="general.label.messagesPoseidonTransactions" />
                          </InputLabel>
                        : <SelectDataProcessor index={index} certificate={body.certificate} piiField={pii} handleFieldInput={handledTextField} handleFieldInputUpdate={handleFieldInputUpdate} handleSelectInput={handleSelect} handleInitialState={handleInitialState}></SelectDataProcessor>
                    }
                </div>
            ))}</div>
            <Button
                className={styles.button}
                variant="contained"
                color="primary"
                fullWidth
                size="large"
                onClick = {
                    () => sendPii(body.params.redirect_url, body.params.pii_types, body.certificate)
                }
                type="button">Grant access
            </Button>
            </div>
        case 'privacy_notification' :

            return <div>
            <p>
                The following data was requested from you: <b>{body.params.pii_types.replace(/,/g, ', ')}</b>.<br /><br />
                This warning message appeared because {body.params.reason.toLowerCase()}
            </p>
            <Button
            className={styles.button}
            variant="contained"
            color="primary"
            fullWidth
            size="large"
            onClick = {
                () => checkProcessor("")
            }
            type="button">
                <FormattedMessage id="general.label.checkDataProcessor" />
            </Button>
            </div>
        case 'risk_notification':
            var dataProcessors = JSON.parse(body.params.data_processors)
            var reputation = JSON.parse(body.params.reputation)
            var theList = [];

            for (let i = 0; i < dataProcessors.length; i++) {
                theList.push(<li>{getDataProcessorName(dataProcessors[i])}: {reputation[i]}</li>)
            }

            return <div>
                <p>
                    You have requested the risk reputation for the following data processors:
                </p>
                <ul>
                    {theList}
                </ul>
            </div>
        case 'warning_notification':
            var dataProcessors = JSON.parse(body.params.data_processors)
            var theList = [];

            for (let i = 0; i < dataProcessors.length; i++) {
                theList.push(<li>{getDataProcessorName(dataProcessors[i])}</li>)
            }

            return <div>
                <p>
                    You have warning notification for the following data processors:
                </p>
                <ul>
                    {theList}
                </ul>
            </div>
        default:
            return <h1>Message is not handled yet</h1>;
    }
}



const MessageContent = ({body, user}) => {

    const dispatch = useDispatch();
    const [fieldValueInput, setFieldValueInput] = useState([]);
    const [selectValueInput, setSelectValueInput] = useState ([]);
    const [typingTimeout, setTypingTimeout] = useState(0)
    const [initialPiiState, setInitialPiiState] = useState([]);

    const handleInitialState = (fieldName, initalValue) => {
        let initialTextFieldArray = fieldValueInput;
        let newInitialItem ={
            field: fieldName,
            value: initalValue
        }
        let piiIndex = initialPiiState.findIndex(pii => pii.field === fieldName);
        if(piiIndex < 0){
            initialTextFieldArray.push(newInitialItem)
            setInitialPiiState(initialTextFieldArray)
        }
    }


    const handleSelect = (e) => {
        let selectArray = selectValueInput;
        let newSelectItem = {
            field: e.target.name,
            value: e.target.value
        }
        if(selectArray.find(name => name.field === newSelectItem.field)){
            let index = selectArray.findIndex(pii => pii.field === e.target.name)
            selectArray[index] = newSelectItem
        } else if (fieldValueInput.find(name => name.field === newSelectItem.field)){
            let textFieldArray = fieldValueInput;
            let index = textFieldArray.findIndex(pii => pii.field === e.target.name)
            textFieldArray.splice(index,1)
            setFieldValueInput(textFieldArray)
            selectArray.push(newSelectItem)
        } else {
            selectArray.push(newSelectItem)
        }
        setSelectValueInput(selectArray)
    }

    const handledTextField = (e) => {
        let textFieldArray = fieldValueInput;
        let newTextFieldItem = {
            field: e.target.name,
            value: e.target.value
        }

        if(textFieldArray.find(name => name.field === newTextFieldItem.field)){
            let index = textFieldArray.findIndex(pii => pii.field === e.target.name)
            textFieldArray[index] = newTextFieldItem
        } else if(selectValueInput.find(name => name.field === newTextFieldItem.field)){
            let selectArray = selectValueInput;
            let index = selectArray.findIndex(pii => pii.field === e.target.name)
            selectArray.splice(index,1)
            setSelectValueInput(selectArray)
        } else {
            textFieldArray.push(newTextFieldItem)
        }
        setFieldValueInput(textFieldArray)
    }

    const handleFieldInputUpdate = (value, id) =>{
        dispatch({ type: EDIT_PII, payload: {pii:value, field: id, certificate: body.certificate} })
    }

    const sendPii = (url) => {
        dispatch({ type: GRANT_ACCESS_PII, piiArray: body.pii_array, redirectUrl: url, directPii: fieldValueInput, sharePii: selectValueInput, dpCertificate: body.certificate, initialPiiState })
    }

    const getPiiValues = (cert) => {
        dispatch({ type: GET_FIELDS, certificate: cert})
    }

    const checkProcessor = (cert) => {
        window.location.pathname = `organisations/${cert}`
    }


    return(
        <Fragment>
            {renderMessage(body, user, sendPii, getPiiValues, handleSelect, handledTextField, checkProcessor, handleFieldInputUpdate, handleInitialState)}
        </Fragment>
        );
}

export default MessageContent