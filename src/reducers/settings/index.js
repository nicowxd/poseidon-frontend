export const RECOVERY_PASSPHRASE = 'RECOVERY_PASSPHRASE';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';

const initialState = {
  passphrase: "",
  recoveryPassphrase: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_PASSWORD:
      return {
        ...state,
        passphrase: action.passphrase,
        newPassphrase: action.newPassphrase,
      };
      // gets recovery pass after changed the password
    case RECOVERY_PASSPHRASE:
      return {
        recoveryPassphrase: action.payload.recovery,
      };
    default:
      return state;
  }
};

export const changePassword = () => ({
  type: CHANGE_PASSWORD
});
