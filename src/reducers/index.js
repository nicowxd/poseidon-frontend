import auth from './auth';
import dataFields from './dataFields';
import organisation from './organisation';
import organisations from './organisations';
import messages from './messages';
import notifications from './notifications';
import summary from './summary';
import user from './user';
import tableOfContent from './tableOfContent'
import settings from './settings';
import messageContent from './messageContent';

export default {
  auth,
  dataFields,
  notifications,
  messages,
  organisation,
  organisations,
  summary,
  user,
  tableOfContent,
  settings,
  messageContent
}
