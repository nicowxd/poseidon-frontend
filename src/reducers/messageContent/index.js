export const GRANT_ACCESS_PII = 'GRANT_ACCESS_PII';
export const GRANT_ACCESS = 'GRANT_ACCESS';
export const UPDATE_ACCESS_PII = 'UPDATE_ACCESS_PII';


const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case GRANT_ACCESS:
      return {
        ...state
      }
    case UPDATE_ACCESS_PII:
      return {
        ...state
      }
    default:
        return state;
  }
};

export const grantAccessPii= (url="", directPiis=[], sharedPiis=[], cert="", initialPiis=[]) => ({
  type: GRANT_ACCESS_PII,
  redirectUrl: url,
  directPii: directPiis,
  sharePii: sharedPiis, 
  dpCertificate: cert,
  initialPii: initialPiis
});

export const updatePii= (sharedPiis=[], cert="") => ({
  type: UPDATE_ACCESS_PII,
  sharePii: sharedPiis, 
  dpCertificate: cert
});