export const DATA_PROCESSORS_GET_PROCESSORS = 'DATA_PROCESSORS_GET_PROCESSORS';
export const DATA_PROCESSORS_SET_PROCESSORS = 'DATA_PROCESSORS_SET_PROCESSORS';

const initialState = {
  processors: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case DATA_PROCESSORS_SET_PROCESSORS:
      return {
        ...state,
        processors: action.payload
      };
    default:
      return state;
  }
};

export const getOrganisations = () => ({
    type: DATA_PROCESSORS_GET_PROCESSORS
});
