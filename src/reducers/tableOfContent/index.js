export const TOC_GET_TOC = 'TableOfContent_GET_TableOfContent';
export const TOC_SET_TOC = 'TableOfContent_SET_TableOfContent';

const initialState = {
  toc: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TOC_SET_TOC:
      return {
        ...state,
        toc: action.payload
      };
    default:
      return state;
  }
};

export const getToc = () => ({
  type: TOC_GET_TOC
});
