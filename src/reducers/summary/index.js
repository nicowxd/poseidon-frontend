export const SUMMARY_GET_SUMMARY = 'SUMMARY_GET_SUMMARY';
export const SUMMARY_SET_SUMMARY = 'SUMMARY_SET_SUMMARY';

const initialState = {
  requests: 0,
  organisations: 0,
  permissions: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SUMMARY_SET_SUMMARY:
      return {
        ...state,
        ...action.payload,
        organisations: action.payload.organisations,
        requests: action.payload.requests,
        permissions: action.payload.permissions
      };
    default:
      return state;
  }
}

export const getSummary = () => ({
  type: SUMMARY_GET_SUMMARY
});
