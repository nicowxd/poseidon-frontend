export const AUTH_AUTHENTICATE = 'AUTH_AUTHENTICATE';
export const AUTH_CLEAR_STATE = 'AUTH_CLEAR_STATE';
export const AUTH_GET_STATUS = 'AUTH_GET_STATUS';
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const AUTH_SET_STATUS = 'AUTH_SET_STATUS';

const initialState = {
  authProvider: false,
  blockchain: false,
  certificate: '',
  invalidPassphrase: false,
  isSubmitting: false,
  isRegistered: false,
  firstLogin: false,
  firstRecovery: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTH_SET_STATUS:
      return {
        ...state,
        ...action.payload
      };
    case AUTH_CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export const authenticate = (passphrase, register = false, recovery, recoveryPassphrase, firstLogin, firstRecovery) => ({
  type: AUTH_AUTHENTICATE,
  passphrase,
  register,
  recovery,
  recoveryPassphrase,
  firstLogin,
  firstRecovery,
});

export const getAuthStatus = () => ({
  type: AUTH_GET_STATUS
});

export const login = () => ({
  type: AUTH_LOGIN
});

export const logout = () => ({
  type: AUTH_LOGOUT
});

export const resetValidation = () => ({
  type: AUTH_SET_STATUS,
  payload: {
    invalidPassphrase: false
  }
});

export const checkRegistered = () => ({
  type: AUTH_SET_STATUS,
  payload: {
    isRegistered: false
  }
});

