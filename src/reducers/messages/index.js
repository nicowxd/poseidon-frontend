export const MESSAGES_GET_MESSAGES = 'MESSAGES_GET_MESSAGES';
export const MESSAGES_SET_MESSAGES = 'MESSAGES_SET_MESSAGES';
export const MESSAGES_GET_NR_MESSAGES = 'MESSAGES_GET_NR_MESSAGES';
export const MESSAGES_SET_NR_MESSAGES = 'MESSAGES_SET_NR_MESSAGES';
export const MARK_AS_READ = 'MARK_AS_READ';
export const UPDATE_MESSAGE_ID_ARRAY = 'UPDATE_MESSAGE_ID_ARRAY';
export const GET_MESSAGE_ID_ARRAY = 'GET_MESSAGE_ID_ARRAY';
export const MESSAGES_GET_NR_PAGES = 'MESSAGES_GET_NR_PAGES';
export const MESSAGES_SET_NR_PAGES = 'MESSAGES_SET_NR_PAGES';
export const GIVE_ACCESS = 'GIVE_ACCESS';
export const GET_LIST_OF_DATA_PROCESSORS = 'GET_LIST_OF_DATA_PROCESSORS';
export const SET_LIST_OF_DATA_PROCESSORS = 'SET_LIST_OF_DATA_PROCESSORS';
export const SET_CURRENT_PAGE= 'SET_CURRENT_PAGE';
export const GET_CURRENT_PAGE= 'GET_CURRENT_PAGE';



const initialState = {
  messages: [],
  nrOfMessages: 0,
  messageIds: {},
  pages : 0,
  currentPage: 1,
  listOfDataProcessors: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case MESSAGES_SET_MESSAGES:
      let unrMsg = 0;
      for(let index in action.payload){
        if(action.payload[index].read === false){
          unrMsg++;
        }
      }
      state.nrOfMessages = unrMsg;
      for(let i=0;i<action.payload.length;i++)
        state.messageIds[action.payload[i].id]=action.payload[i].read;
      return {
        ...state,
        messages: action.payload,
      };
    case MESSAGES_SET_NR_MESSAGES:
      return {
        ...state, 
      };
    case MESSAGES_SET_NR_PAGES:
      return  {
        ...state,
        pages: action.payload.pages,
        nrOfMessages: action.payload.unrMsg,
      };
    case MARK_AS_READ:
      return  {
        ...state,
      };
    case UPDATE_MESSAGE_ID_ARRAY:
      if(state.messageIds[action.payload] === false) {
        state.messageIds[action.payload] = true;
        state.nrOfMessages--;
      }
      return  {
        ...state,
      };
      case SET_LIST_OF_DATA_PROCESSORS:
        return {
          ...state,
          listOfDataProcessors: action.payload
        };
      case SET_CURRENT_PAGE:
        return{
          ...state,
          currentPage: action.payload
        }
    default:
      return state;
  }
};

export const getMessages = (page=1, itemsPerPage=15) => ({
  type: MESSAGES_GET_MESSAGES,
  page: page,
  itemsPerPage: itemsPerPage
});

export const markMessageAsRead = (id) => ({
  type: MARK_AS_READ,
  id
});

export const getNrOfMessages = () => ({
  type: MESSAGES_GET_NR_MESSAGES
});

export const getNrOfPages = () => ({
  type: MESSAGES_GET_NR_PAGES
});

export const giveAccess= () => ({
  type: GIVE_ACCESS
});

export const getDataProcessorList = (field="") => ({
  type: GET_LIST_OF_DATA_PROCESSORS,
  field: field,
});

export const getCurrentPage = (page) => ({
  type: GET_CURRENT_PAGE,
  page: page,
});
