export const ORGANISATION_CLEAR_DATA = 'ORGANISATION_CLEAR_DATA';
export const ORGANISATION_GET_DATA = 'ORGANISATION_GET_DATA';
export const ORGANISATION_STORE_DATA = 'ORGANISATION_STORE_DATA';
export const REVOKE_PERMISSION = 'REVOKE_PERMISSION';
export const HAS_NOTIFICATION = 'HAS_NOTIFICATION';
export const GET_PII_TYPES = 'GET_PII_TYPES';
export const REVOKE_ALL_PERMISSIONS = 'REVOKE_ALL_PERMISSIONS'


const initialState = {
  certificate: '',
  hasNotification: false,
  pii_array: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ORGANISATION_STORE_DATA:
      return {
        ...state,
        ...action.payload
      };
    case ORGANISATION_CLEAR_DATA:
      return initialState;
    case REVOKE_PERMISSION:
      return initialState;
    case REVOKE_ALL_PERMISSIONS:
      return initialState;
    case HAS_NOTIFICATION:
      return {
        ...state,
        ...action.payload
      };
    case GET_PII_TYPES:
      return  {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};


export const getOrganisationData = certificate => ({
  type: ORGANISATION_GET_DATA,
  certificate
});

export const clearOrganisationData = () => ({
  type: ORGANISATION_CLEAR_DATA
});

export const onRevoke = (field, certificate) => ({
  type: REVOKE_PERMISSION,
  field: field,
  certificate: certificate
});

export const revokeAllPermissions = (cert) => ({
  type: REVOKE_ALL_PERMISSIONS,
  cert: cert
});

export const checkHasNotification = () => ({
  type: HAS_NOTIFICATION,
  payload: {
    hasNotification: true
  }
});
