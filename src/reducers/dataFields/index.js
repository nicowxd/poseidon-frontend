export const FIELD_STATUS_ACTIVE = 'active';
export const FIELD_STATUS_REQUESTED = 'requested';
export const SET_PII = 'SET_PII';
export const RESET_PII = 'RESET_PII';
export const EDIT_PII = 'EDIT_PII';
export const GET_FIELDS = 'GET_FIELDS';
export const SET_FIELDS = 'SET_FIELDS';
export const GET_FIELD_VALUE = 'GET_FIELD_VALUE';
export const SET_FIELD_VALUE = 'SET_FIELD_VALUE';

const initialState = {
  pii_array: [],
  piiObjects: {},
  fields: [],
  fieldValue: [],
  permissions: {},
  dataProcessorPii: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PII:
      return  {
        ...state,
        pii_array: action.payload.pii_array,
        piiObjects: action.payload.piiObjects
      };
    case RESET_PII:
      return {
        ...initialState,
        pii_array: []
      };
    case EDIT_PII:
      return {
        ...state,
        ...action.payload
      };
    case SET_FIELDS:
      return{
        ...state,
        fields: action.payload.normalizedFields,
        fieldValue: action.payload.fieldValues,
        permissions: action.payload.fields
      }
    default:
      return state;
  }
};

export const getFields = (certificate) => ({
  type: GET_FIELDS,
  certificate: certificate
});