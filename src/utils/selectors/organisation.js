export const getOrganisation = (state, certificate) => {
  const { organisations: { processors } } = state;

  return processors.find(organisation => organisation.certificate === certificate);
};

