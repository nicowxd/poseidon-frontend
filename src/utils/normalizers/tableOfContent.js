// TODO: Temp mock data

const TOC = [
  {
    chapter: 'Chapter 1',
    subject: 'Registration and execution of Contract',
  },
  {
    chapter: 'Chapter 2',
    subject: 'Terms Duration and Termination for withdrawal',
  },
  {
    chapter: 'Chapter 3',
    subject: 'Services and using the Platform',
  },
  {
    chapter: 'Chapter 4',
    subject: 'Obligations and Responsibilities of the User',
  },
  {
    chapter: 'Chapter 5',
    subject: 'Warranties and Limitations of liability of the Platform',
  },  {
    chapter: 'Chapter 6',
    subject: 'Intellectual Property Rights',
  },  {
    chapter: 'Chapter 7',
    subject: 'Applicable Law and Jurisdiction Disputes',
  },  {
    chapter: 'Chapter 8',
    subject: 'Privacy',
  },  {
    chapter: 'Chapter 9',
    subject: 'Approval of the specific clauses',
  },
];

export default TOC;
