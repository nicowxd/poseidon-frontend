export const normalizeToState = ({ auth_provider, blockchain }) => ({
  authProvider: auth_provider,
  blockchain
});
