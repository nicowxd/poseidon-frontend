export default (localeData) => {
  const language = (navigator.languages && navigator.languages[0]) || navigator.language;
  const languageWithoutRegionCode = (language || '').toLowerCase().split(/[_-]+/)[0];
  // const messages = localeData[languageWithoutRegionCode] || localeData[language] || localeData.en;

  return localeData[languageWithoutRegionCode] || localeData[language] || localeData.en;
}
