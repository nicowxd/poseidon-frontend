import * as React from 'react';
import memoize from 'lodash.memoize';

const MIN_DESKTOP_WIDTH = 560; // TODO: this should be exported from scss file
const getWindowWidth = () => window.innerWidth;
const getWindowHeight = () => window.innerHeight;

class ScreenSize extends React.Component {
  componentDidMount = () => window.addEventListener('resize', this.onUpdateScreen);
  componentWillUnmount = () => window.removeEventListener('resize', this.onUpdateScreen);

  onUpdateScreen = () => {
    this.props.onUpdateScreen({
      screen: {
        width: getWindowWidth(),
        height: getWindowHeight()
      }
    })
  };

  render = () => this.props.children;
}

export const isDesktopScreen = memoize((width) => width > MIN_DESKTOP_WIDTH);

export const withScreenSize = (ComposedComponent) =>
  class extends React.Component {
    state = {
      screen: {
        width: getWindowWidth(),
        height: getWindowHeight()
      }
    }

    onUpdateScreen = (state) => this.setState(() => ({ ...state }));

    render = () => (
      <ScreenSize onUpdateScreen={this.onUpdateScreen}>
        <ComposedComponent
          {...this.props}
          screen={this.state.screen}
        />
      </ScreenSize>
    )
  };
