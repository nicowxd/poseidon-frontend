import { login } from 'reducers/auth';
import { addNotification } from 'reducers/notifications';

export const handleBackendError = (err) => {
  const { status: httpStatus = 500 } = err;
  const { error: message } = err;
  const messageId = {
    500: 'api.error.general',
    503: 'api.error.503'
  };

  if (httpStatus == 401) {
    console.log('Session expired. Logging you out.')
    return login();
  }

  return addNotification({
      id: 'backend_error_notification',
      error: true,
      autoClear: false,
      message,
      messageId: messageId[httpStatus] || (!message && messageId[500]),
  });
};
