import React from "react";
import Hidden from "@material-ui/core/Hidden";

import PageContent from "components/Layout/PageContent";
import MessagesList from "containers/MessagesList";
import MessageCard from "containers/MessageCard";
import Button from "components/Button";
import { Link } from "react-router-dom";

import styles from "./index.module.scss";

import { MESSAGES_PATH } from "constants.js";

export default ({ match: { params } }) => {
  const { id: messageId } = params;

  const backToMessages = () => {};

  return (
    <div className={styles.page}>
      {messageId ? (
        <Hidden smDown>
          <PageContent>
            <MessagesList activeId={messageId} />
          </PageContent>
        </Hidden>
      ) : (
        <PageContent>
          <MessagesList activeId={messageId} />
        </PageContent>
      )}

      {messageId && (
        <PageContent className={styles.content}>
          <MessageCard id={messageId} />
          <Link to={MESSAGES_PATH}>
            <Button
              className={styles.button}
              variant="contained"
              color="primary"
              onClick={backToMessages}
            >
              Back
            </Button>
          </Link>
        </PageContent>
      )}
    </div>
  );
};
