import React from 'react';

import PageContent from 'components/Layout/PageContent';
import TableOfContent from 'containers/TableOfContent';
import Terms from 'components/TacContent/';


import styles from './index.module.scss';

export default () => (
  <div className={styles.page}>
    <PageContent >
      <TableOfContent />
    </PageContent>
    <PageContent >
      <Terms />
    </PageContent>
  </div>
);