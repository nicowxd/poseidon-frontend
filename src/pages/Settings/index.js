import React from 'react';
import PageContent from 'components/Layout/PageContent';
import Settings from "../../containers/Settings";

export default () => (
  <div>
    <PageContent >
      <Settings />
    </PageContent>
  </div>
);
