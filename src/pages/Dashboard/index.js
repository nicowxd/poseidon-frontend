import React from 'react';

import PageContent from 'components/Layout/PageContent';
import SummaryCard from 'containers/SummaryCard';
import ExchangeReports from 'containers/ExchangeReports';
import FirstLogin from 'containers/FirstLogin';

import styles from './index.module.scss';

export default () => (
  <PageContent>
    <FirstLogin className={styles.section} />
    <SummaryCard className={styles.section} />
    <ExchangeReports className={styles.section} />
  </PageContent>
);
