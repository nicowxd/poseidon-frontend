import React from "react";
import { Switch, Route } from "react-router-dom";

import PageContent from "components/Layout/PageContent";
import OrganisationView from "containers/OrganisationView";
import OrganisationsList from "containers/OrganisationsList";

import styles from "./index.module.scss";

const SinglePage = props => (
  <PageContent className={styles.fullWidth}>
    <OrganisationView {...props} />
  </PageContent>
);

const ListPage = props => (
  <PageContent>
    <OrganisationsList {...props} />
  </PageContent>
);

export default ({ match: { path } }) => (
  <Switch>
    <Route exact path={`${path}/:certificate`} component={SinglePage} />
    <Route path={path} component={ListPage} />
  </Switch>
);
