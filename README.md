# PoSeID-on Dashboard front-end

## Available Scripts

Minimum requirements:

* `node v8.10.0`
* `yarn 1.17.3`

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode and uses the `API_HOST` variable from
`./src/setupProxy` to proxy `/api` requests. Open
[http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `yarn start:dev` runs the app with `https=true`

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

See the section about [running
tests](https://facebook.github.io/create-react-app/docs/running-tests) for more
information.

### `yarn build`

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the
best performance.

The build is minified and the filenames include the hashes. Your app is ready to
be deployed!

See the section about
[deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.

### Build Image

    make && make push

*if you have a `poseidon-platform` minikube running you can deploy the new image with:*

    kubectl delete pods -l app=poseidon-frontend

### More

This project was bootstrapped with [Create React
App](https://github.com/facebook/create-react-app).

You can learn more in the [Create React App
documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Cypress Tests

### Requirements

To run the following tests you will need
[node](https://nodejs.org/en/download/), and the poseidon minikube running.

### Usage

To check tests manually run the following command. This will open the GUI for
cypress.

    yarn cypress:start

To run the tests automatically run the following command.

    yarn cypress:tests

This will run the tests from command line and will generate HTML reports in
`cypress/reports/`, it will also record the tests is `cypress/videos/`.

### Contributing

To write a new tests open the project and create a new test file in
`cypress/integration`. Write the tests accordingly to
[cypress](https://docs.cypress.io).

For more information visit [Cypress official website](https://docs.cypress.io)

## Legal

Copyright (C) 2021 Jibe Company B.V.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
